# dasg documentation

# Crash course

### Function 

As an example, we consider the following polynomial map

    p:  [0, 1]^3   ----->  IR
        (x, y, z)  |---->  x^3 * y + z

To implement this polynomial, we must implement the abstract interface ``Function`` from ``dasg.abc``.

    from dasg.abc import Function
    from dasg.domain import hyper

    class Polynom(Function):
        @property
        def domain(self):
            return hyper([0,1], 3)

        @property
        def dof(self):
            return 1

        def evaluate(self, point):
            x, y, z = point[0], point[1], point[2]
            return x**3 * y + z

- ``domain`` defines the domain, any compact domain in form of a dictionary like 

        {0: [0,  3],  
         1: [-2, 1.7]}

    is valid. For hypercube or hyperrectangle domains we can use the convenient ``hyper`` function:

        hyper([0,1], 3) = {0: [0,1], 
                           1: [0,1],
                           2: [0,1]}

- ``dof`` defines the dimensionality of the codomain, any positive integer is valid.
    For the example map ``p`` we use ``1`` since we map onto the reals, and e.g. for mapping onto complex numbers we would choose ``2``.
    The name is short for degree of freedom which comes from finite element theory as ``dasg`` was intended for integration FEM functions.
    
- ``evaluate`` defines the map itself and should be self-explanatory.

### Usage

We can approximate the function with the following code

    from dasg import Grid
    from Polynom import Polynom             # the user defined function from above

    polynom = Polynom()                         
    grid = Grid(polynom, tolerance=1E-3)    # approximates the function

The only mandatory setting is

- ``tolerance``: error reduction to achieve. Smaller values will approximate the function more accurate. To be more precise, this means that the approximation is considered as converged if the approximated volume changes less than ... compared to the volume of the first level (for details see [GG03]). Note that this doesn't mean that the approximation has absolute error of the given value, for that you must use the RMSE function (explained below).

Optional keyword arguments are

- ``maxIter``: stop after ... iterations, no matter if ``tolerance`` has been reached. Defaults to 100.
- ``adaptiveWeight``: float in [0,1], defaults to 1. The choice of 1 corresponds to a greedy error reduction (refines the grid only considering error reduction), where 0 corresponds to a non-adaptive sparse grid (only involved work matters). As ``sandbox/compare-weighting`` suggests, a greedy selection usually performs better. This setting has no effect if the function has a one-dimensional domain.
- ``maximalLevel``: specifies how many overall refinement levels can be used. Defaults to the maximal possible refinement (compare the limitations section below).
- ``maximalLevelPerDimension``: specifies the maximal refinement per dimension, defaults to the maximum 11.
- ``nCPU``: number of CPU cores used to perform parallelization of the grid generation and the RMSE test (see below). Defaults to the number of cores available. Ignored if ``joblib`` isn't installed, or only one core is available.

After the grid is approximated, we can use the following methods

- ``info = grid.info()``: output information about the grid generation:
    - ``gridpoints``: number of gridpoints generated
    - ``maximalRefinement``: maximal refinement per dimension
    - ``relativeError``: relative error reached
    - ``flags``: indicates the success of the grid generation.
        - ``0``: converged (reached ``tolerance``)
        - ``1``: didn't converge after ``maxIter`` iterations
        - ``2``: didn't converge within maximal possible refinement

    Flag ``1`` indicates that we need to choose a higher ``maxIter`` value, and flag ``2`` indicates that the options ``maximalLevel`` and/or ``maximalLevelPerDimension`` were chosen too small.

- ``volume = grid.integrate()``: integrate the function (get it's volume)
- ``value = grid.interpolate(x)``: interpolate a domain point ``x`` (either as numpy array, or list of length ``d``)
- ``rmse = grid.rmse(100)``: compute the root mean squared error of 100 Monte Carlo points - meaning we compare the interpolations of this points with the deterministic solutions. Depending on your function this doesn't necessarily give information about the quality of the volume approximation (compare ``sandbox/compare-quadRules/{Polynom, Genz}``)
- ``grid.refine(tolerance=1E-5, maxIter=500)``: refine the grid further by specifying a smaller ``tolerance`` (e.g. if the RMSE isn't satisfying), or a larger ``maxIter`` (e.g. if we encounter flag ``1``). You may also specify only one of these arguments. 

## More on functions

### Overriding abstract interface

In the above example we have implemented the abstract interface ``Function`` from ``dasg.abc``. 
To be precise, the abstract interface ``Function`` is a partially implemented class, which provides the properties

- ``norm`` acting on the codomain.
    Is used for the adaptive grid refinement progress as well as the Monte-Carlo error, defaults to

        def norm(self, value):
            return numpy.linalg.norm(value, 2)

    See ``examples/Poisson/Poisson.py`` where we use the H1 norm acting on a codomain being a Finite Element space.

- ``initialLevel`` to start the grid generation.
    Defaults to a singular point in the domain center

        @property
        def initialLevel(self):
            return numpy.ones(self.d, dtype=int)

    This property should only be overridden if ``dasg`` reports a ``ZeroDivisionError``!
    Reason being that evaluation of the first gridpoint(s) must not vanish in the specified norm (compare [GG03]).
    An example where we need to override this property is given by the following function

        f:  [-1, 1]^3  ----->  IR
            (x, y, z)  |---->  x * (y + 2) * z

    Clearly, ``f`` vanishes whenever ``x`` and/or ``z`` are ``0``, thus the domain center ``(0,0,0)`` vanishes. 
    Hence for this map, we must override the property

        @property
        def initialLevel(self):
            return numpy.array([2, 1, 2], dtype=int)

    Make sure to return an array of datatype ``int``.

- ``d`` dimensionality of the domain, defaults to

        @property
        def d(self):
            return len(self.domain)

    and should never be overwritten.

### Matrix functions

For functions mapping onto a matrix codomain, we can always return a flattened evaluation of the matrix:

    def evaluate(self, x):
        matrix = evaluate_function_at(x)
        return matrix.flatten()

but then also ``grid.evaluate(x)`` will return a flattened array which has to be reshaped to a matrix.

If the matrix is sparse and the evaluated matrices possess the same sparsity structure, it is more economical (from a RAM point of view) to exploit the sparsity.
For doing so, we only need to make two modifications.

- instead of ``Grid`` we use ``MatrixGrid``

        from dasg import MatrixGrid
        grid = MatrixGrid(matrixFunction, tolerance=1E-3)

- instead of implementing the abstract interface ``Function`` we implement ``MatrixFunction``.
    This is very similar to the above discussed, we only need to implement a further property specifying the compressed sparse row format of the matrices 

        @property
        def CSR(self):
            r, c, nonZeroValues = matrix.data     # compare scipy.sparse.csr_matrix
            return {'r': r, 'c': c}

    and provide the sparse data of the matrix as the return value of ``evaluate``:

        def evaluate(self, x):
            matrix = evaluate_function_at(x)
            r, c, nonZeroValues = matrix.data     # compare scipy.sparse.csr_matrix
            return nonZeroValues
            
As an example consult ``examples/Matrix``.

## Quadrature rule

``dasg`` can be used with variable quadrature rules.
A valid quadrature rule consists of hierarchical basis functions with local support and boundaryless collocation points.

Currently implemented are

- local high order polynomials with Gauss collocation points

    ![Gauss local high order polynomials](../sandbox/plot-quadRules/plots/LHOP_Gauss.png)

- hat basis functions with equidistant collocation points

    ![Equidistant hat](../sandbox/plot-quadRules/plots/Hat_Equidistant.png)

which are plotted for levels 1 to 4 on their default domains.

Since local high order polynomials exploit the smoothness of a function, they usually perform better (see ``examples/compare-quadRules/``).
Hence they are the default choice, but we can use hat basis functions with

    from dasg.quadrule import Hat_Equidistant
    
    hatRule = Hat_Equidistant(polynom)
    grid = Grid(polynom, tolerance=1E-3, quadRule=hatRule)

In case you want to implement another quadrature rule, consult the abstract interface ``dasg/abc/QuadratureRule.py`` (pull requests welcome).



## What else to know

### Logging

``dasg`` provides logging of the grid generation.
We can setup a text logger logging to a file with

    from dasg.logging import setup_text_logger
    logger = setup_text_logger()

and may override the following default settings

    logSettings = {'outDir': 'log',             # directory to output the logs
                   'textFile': text.log}        # output file of text log

by specifying

    logger = setup_text_logger(logSettings)

Furthermore we can plot information about the grid generation which is enabled by creating the grid with

    grid = Grid(function, tolerance=1E-3, logSettings=True)

Instead of ``logSettings=True`` we can also say ``logSettings={}``, in both cases the following defaults are used:

    logSettings = {'outDir': 'log',             # directory to output the logs
                   'plotFile': 'data.pdf'       # PDF plot of grid generation progress
                   'dataFile': 'data.json'      # data file holding information necessary for plotting
                   'livePlot': False            # explained below 
                  }

We can override any of those settings, e.g.

    logSettings = {'outDir': 'plots',
                   'livePlot': True}
    grid = Grid(function, tolerance=1E-3, logSettings=logSettings)


If we set ``'livePlot': False``, the progress plot will be created when the grid generation is done. 
By setting this to ``True``, the PDF file will be updated after each iteration - in that case you might want to use a PDF viewer with automatic file reloading (e.g. [zathura](https://pwmt.org/projects/zathura/)).
Note however that live plotting of the PDF file will slow down the grid generation.


### Limitations

``dasg`` uses a data structure to store gridpoints (proposed in [Feu10]), which limits the dimensionality of the function domain to maximal 33.
However, using such high dimensional domains limits the possible refinement of the grid. 
Below we see a plot of how many overall levels are possible in each dimension.
In case our function is strongly coupled, the possible refinement above dimension 30 might be too low to achieve good results.
Make sure to run a Monte-Carlo error test (``grid.rmse(100)``) if you are unsure whether such high-dimensional functions are approximated well.

![Maximal overall refinement per dimension](../sandbox/precompute-plot-maximalLevels/maximalLevels.png)

## Examples
Find examples for functions and main files under

- ``examples/Polynom`` simple 1D polynom
- ``examples/Genz`` multivariante Genz test functions
- ``examples/FEM`` Finite Element problem (stochastic Poisson)
- ``examples/Matrix`` sparse matrix codomain
