# dasg
## dimension adaptive sparse grids

``dasg`` is a package for (high-dimensional) numerical integration and interpolation. 
It implements **d**imension-**a**daptive **s**parse **g**rids of Smolyak type, with an adaptive algorithm along [GG03] and data structures proposed in [Feu10] (see ``docs/Bibliography``).
It supports functions mapping from compact and possibly high-dimensional domains, onto arbitrary finite-dimensional codomains.

The choice of quadrature rule is variable, currently implemented are 

- local high order polynomials with Gaussian collocation points and
- hat basis functions with equidistant collocation points.

The adaptive grid generation is parallelized, as well as the supplied Monte-Carlo error test.

## Installation
See ``INSTALL``

## Usage
``dasg`` is easy to use! See [``docs/Documentation.md``](docs/Documentation.md) for a detailed guide, and dive into ``examples/``.

## License
MIT, see ``LICENSE``

## Citing
If you use ``dasg`` in scientific context please consider citing
    
    @Electronic{dasg,
        Title        = {\texttt{dasg} - Dimension-adaptive sparse grids for high-dimensional integration and interpolation},
        Author       = {Doll, Andreas},
        HowPublished = {\url{https://bitbucket.org/andreasdoll/dasg}},
    }

## TODO

- refactor hashing module (?)
