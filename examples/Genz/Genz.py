from __future__ import division, absolute_import
import numpy as np
from scipy.special import erf

from dasg.abc import Function
from dasg.domain import hyper, transform_to_UnitDomain, transform_from_UnitDomain

class Genz(Function):
    """ Genz test functions as in [CM13], 
        symbolical integrals as in [Nah05].
    """
    def __init__(self, d, functionType):
        assert functionType in ['oscillatory', 'productPeak', 'gaussian', 'continuous'], "Unknown Genz function type!"
        self._domain = hyper([0,1], d)
        self._type = functionType
        self._dof = 1
        self._w = np.random.rand(d) 
        self._c = np.random.rand(d) 

    @property
    def dof(self):
        return self._dof

    @property
    def domain(self):
        return self._domain

    @property
    def w(self):
        return self._w

    @property
    def c(self):
        return self._c

    @property
    def type(self):
        return self._type

    def evaluate(self, x):
        """ Evaluate domain point """
        if self.type == 'oscillatory':
            val = self.oscillatory(x) 
        elif self.type == 'productPeak':
            val = self.productPeak(x)
        elif self.type == 'gaussian':
            val = self.gaussian(x)
        elif self.type == 'continuous':
            val = self.continuous(x)
        return np.array([val])

    def oscillatory(self, x):
        d = self.d
        c = self.c
        w = self.w
        res = 0
        for j in range(d):
            res += c[j]*x[j]
        res = np.cos(2*np.pi*w[0] + res)
        return res
    
    def productPeak(self, x):
        d = self.d
        c = self.c
        w = self.w
        res = 1
        for j in range(d):
            res *= (np.power(c[j], -2) + np.power(x[j]-w[j], 2))
        res = 1/res
        return res
    
    def gaussian(self, x):
        d = self.d
        c = self.c
        w = self.w
        res = 0
        for j in range(d):
            res += np.power(c[j], 2)*np.power(x[j] - w[j], 2)
        res = np.exp(-res)
        return res
    
    def continuous(self, x):
        d = self.d
        c = self.c
        w = self.w
        res = 0
        for j in range(d):
            res += np.power(c[j], 2)*np.power(np.abs(x[j] - w[j]), 2)
        res = np.exp(-res)
        return res
    
    def symbolicVolume(self):
        """ Evaluate domain point """
        if self.type == 'oscillatory':
            return self.symbolic_oscillatory() 
        elif self.type == 'productPeak':
            return self.symbolic_productPeak()
        elif self.type == 'gaussian':
            return self.symbolic_gaussian()
        elif self.type == 'continuous':
            return self.symbolic_continuous()

    def symbolic_oscillatory(self):
        d = self.d
        c = self.c
        w = self.w
        prod1 = 1
        for j in range(d):
            prod1 *= np.sin(c[j]/2)
        prod2 = np.product(c)
    
        res = np.power(2, d)
        res *= np.cos(1/2 * (4*np.pi*w[0] + np.sum(c)) * prod1)
        res *= 1/prod2
        return res
    
    def symbolic_productPeak(self):
        d = self.d
        c = self.c
        w = self.w
        res = 1
        for j in range(d):
            res *= c[j] * (np.arctan(c[j]*(1-w[j])) + np.arctan(c[j]*w[j]))
        return res
    
    def symbolic_gaussian(self):
        d = self.d
        c = self.c
        w = self.w
        res = 1
        for j in range(d):
            res *= np.sqrt(np.pi)/(2*c[j]) * (erf(c[j]*w[j]) - erf(c[j]*(w[j]-1)))
        return res
    
    def symbolic_continuous(self):
        d = self.d
        c = self.c
        w = self.w
        res = 1
        for j in range(d):
            res *= 1/c[j] * (2 - np.exp(-c[j]*w[j]) - np.exp(-c[j]*(1-w[j])))
        return res
