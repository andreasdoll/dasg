from __future__ import division, absolute_import
import numpy as np

from dasg import Grid
from Genz import Genz


def main():
    dim = 5
    function = Genz(dim, 'gaussian')
    grid = Grid(function, tolerance=1E-3)

    volume = grid.integrate()
    print "Vol =", volume[0]
    print "Vol error =", np.abs(volume[0] - function.symbolicVolume())
    print 'Grid generation:'
    for k,v in grid.info().iteritems():
        print "  ", k, v

    rmse = grid.rmse(100)
    print 'RMSE (L2):'
    for k, v in sorted(rmse.iteritems()):
        print "  ", k, v


if __name__ == '__main__':
    main()
