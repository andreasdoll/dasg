from __future__ import division

from dasg import MatrixGrid
from Matrix import Matrix

def main():
    matrix = Matrix(12)

    grid = MatrixGrid(matrix, tolerance=1E-3)
    print 'Grid generation:'
    for k, v in grid.info().iteritems():
        print "  ", k, v

    rmse = grid.rmse(100) 
    print 'RMSE (L2):'
    for k, v in sorted(rmse.iteritems()):
        print "  ", k, v
    

if __name__ == '__main__':
    main()
