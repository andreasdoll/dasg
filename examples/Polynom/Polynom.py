from __future__ import division, absolute_import
import numpy as np

from dasg.abc import Function

class Polynom(Function):
    def __init__(self, domain):
        self._domain = domain
        self._dof = 1

    @property
    def domain(self):
        return self._domain

    @property
    def dof(self):
        return self._dof

    def evaluate(self, x):
        res = 2*np.power(x, 4) + 4*np.power(x, 3) \
              - 3*np.power(x, 2) - 2*x + 3   
        return res

    def symbolicVolume(self):
        def integral(x):
            return  2/5*np.power(x, 5) + np.power(x, 4) \
                    - np.power(x, 3) - np.power(x, 2) + 3*x
        a = self.domain[0][0]
        b = self.domain[0][1]
        res = integral(b) - integral(a)
        return res
