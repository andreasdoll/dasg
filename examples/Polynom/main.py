from __future__ import division, absolute_import
import numpy as np

from dasg import Grid
from dasg.domain import hyper
from Polynom import Polynom

def main():
    domain = hyper([-1,1], 1)
    polynom = Polynom(domain)
    grid = Grid(polynom, tolerance=1E-4)

    volume = grid.integrate()
    print "Vol =", volume[0]
    print "Vol error =", np.abs(volume[0] - polynom.symbolicVolume())
    print 'Grid generation:'
    for k, v in grid.info().iteritems():
        print "  ", k, v

    rmse = grid.rmse(100)
    print 'RMSE (L2):'
    for k, v in sorted(rmse.iteritems()):
        print "  ", k, v



if __name__ == '__main__':
    main()
