from __future__ import division, absolute_import
import logging
import numpy as np

from dasg.abc import Function as AbstractFunction
from dasg.domain import hyper

from dolfin import FunctionSpace, TestFunction, TrialFunction, DirichletBC, UnitSquareMesh, Function, \
                   set_log_level, WARNING, dx, Constant, nabla_grad, inner, solve, Expression, \
                   interpolate, norm, assemble

ffc_logger = logging.getLogger('FFC')
ufl_logger = logging.getLogger('UFL')
ffc_logger.setLevel(logging.WARNING)
ufl_logger.setLevel(logging.WARNING)
set_log_level(WARNING)

logger = logging.getLogger(__name__)


class Poisson(AbstractFunction):
    def __init__(self, d):
        self._domain = hyper([-1,1], d) 
        self._FEM = self._setupFEM()
        self._randomField = RandomField()
        self._warmup()

    @property
    def domain(self):
        return self._domain

    @property
    def dof(self):
        return self.FEM['V'].dim()

    @property
    def FEM(self):
        return self._FEM

    def norm(self, x):
        FEM = self.FEM
        fenics_x = Function(FEM['V'])
        fenics_x.vector().set_local(x)
        return norm(fenics_x, 'H1')

    @property
    def randomField(self):
        return self._randomField

    def _setupFEM(self):
        def u0_boundary(x, on_boundary):
            return on_boundary

        mesh = UnitSquareMesh(15, 15)
        V = FunctionSpace(mesh, 'CG', 1)
        u = TrialFunction(V)
        v = TestFunction(V)
        f = Constant(1) 
        L = f * v * dx
        bc = DirichletBC(V, Constant(0), u0_boundary)
        return {'V': V, \
                'u': u, \
                'v': v, \
                'f': f, \
                'L': L, \
                'bc': bc, \
                'mesh': mesh}

    def _warmup(self):
        _ = self.evaluate(np.repeat(0, self.d))

    def evaluate(self, x):
        FEM = self.FEM
     
        realisation = self.randomField.realisation(x, FEM['V'])
        field = Function(FEM['V'])
        field.vector().set_local(realisation)

        a = field * inner(nabla_grad(FEM['u']), nabla_grad(FEM['v'])) * dx(self.FEM['mesh'])
        A = assemble(a)
        b = assemble(FEM['L'])
        FEM['bc'].apply(A, b)
    
        result = Function(FEM["V"])
        solve(a == FEM['L'], result, FEM['bc'])
        return result.vector().array()


class RandomField(object):
    def __init__(self):
        self._a = Expression('C + cos(A*pi*F1*x[0]) * cos(A*pi*F2*x[1])', \
                             A=1, C=1, F1=0, F2=0)

    @property
    def a(self):
        return self._a

    def realisation(self, x, V):
        def indexer(i):
            m1 = np.floor(i/2)
            m2 = np.ceil(i/2)
            return m1, m2

        a = self.a
        a.C = 1
        a.F1 = 0
        a.F2 = 0
        field = interpolate(a, V).vector().array()
        a.C = 0

        for j, x_j in enumerate(x):
            eigenValue = 1/(j+1)
            a.F1, a.F2 = indexer(j+2)
            field += eigenValue * x_j * interpolate(a, V).vector().array()
        return field
