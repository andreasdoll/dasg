from __future__ import division

from dasg import Grid
from Poisson import Poisson

def main():
    fem = Poisson(7)

    grid = Grid(fem, tolerance=1E-3)
    print 'Grid generation:'
    for k, v in grid.info().iteritems():
        print "  ", k, v

    rmse = grid.rmse(100) 
    print 'RMSE (H1):'
    for k, v in sorted(rmse.iteritems()):
        print "  ", k, v


if __name__ == '__main__':
    main()
