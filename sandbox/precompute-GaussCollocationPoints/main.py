from __future__ import division, absolute_import
from multiprocessing import cpu_count

from dasg.quadrule.lhop_gauss.precompute import precompute_data

def main():
    precompute_data(level=11, \
                    nCPU=cpu_count())
    
if __name__ == '__main__':
    main()
