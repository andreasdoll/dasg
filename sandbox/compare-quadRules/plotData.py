from __future__ import division, absolute_import
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import os
import json

def main():
    if not os.path.exists('plots/'):
        os.makedirs('plots/')
    dataFiles = os.listdir('data/')
    for dataFile in dataFiles:
        data = readData('data/%s'%dataFile)
        if 'Polynom' in dataFile or 'Genz' in dataFile:
            plot(data)
        else:
            plot_without_volume(data)

def readData(fileName):
    with open(fileName, 'r') as f:
        content = json.load(f)
    return content

def plot(data):
    d = data['d']
    title = data['title']
    fileName = data['fileName']
    lhop = data['lhop']
    hat = data['hat']

    lines = ['-', '--']
    fig = plt.figure()
    fig.set_size_inches(8.27, 11.7)
    ax = fig.add_subplot(211)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.title('%s (%sD): volume error'%(title, d))
    ax.set_xlabel("#gridpoints")
    ax.set_ylabel("volume error")
    for c, (measure, error) in enumerate(sorted(lhop['volume'].iteritems())):
        ax.plot(lhop['nPoints'], error, color='blue', linestyle=lines[c], linewidth=2, marker='o', markersize=10, fillstyle='none', label='LHOP (%s)'%measure)
    for c, (measure, error) in enumerate(sorted(hat['volume'].iteritems())):
        ax.plot(hat['nPoints'], error, color='red', linestyle=lines[c], linewidth=2, marker='o', markersize=10, fillstyle='none', label='Hat (%s)'%measure)
    ax.legend(loc="upper right", prop={'size':12})

    ax = fig.add_subplot(212)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.title('%s (%sD): RMSE'%(title, d))
    ax.set_xlabel("#gridpoints")
    ax.set_ylabel("RMSE")
    for c, (measure, error) in enumerate(sorted(lhop['rmse'].iteritems())):
        ax.plot(lhop['nPoints'], error, color='blue', linestyle=lines[c], linewidth=2, marker='o', markersize=10, fillstyle='none', label='LHOP (%s)'%measure)
    for c, (measure, error) in enumerate(sorted(hat['rmse'].iteritems())):
        ax.plot(hat['nPoints'], error, color='red', linestyle=lines[c], linewidth=2, marker='o', markersize=10, fillstyle='none', label='Hat (%s)'%measure)
    ax.legend(loc="upper right", prop={'size':12})
    fig.tight_layout()
    fileName = os.path.join('plots', '%s-%sD.pdf'%(fileName, d))
    pdf = PdfPages(fileName)
    pdf.savefig(fig)
    pdf.close()
    plt.close(fig)

def plot_without_volume(data):
    d = data['d']
    title = data['title']
    fileName = data['fileName']
    lhop = data['lhop']
    hat = data['hat']

    lines = ['-', '--']
    fig = plt.figure()
    fig.set_size_inches(8.27, 11.7)

    ax = fig.add_subplot(111)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.title('%s (%sD): RMSE'%(title, d))
    ax.set_xlabel("#gridpoints")
    ax.set_ylabel("RMSE")
    for c, (measure, error) in enumerate(sorted(lhop['rmse'].iteritems())):
        ax.plot(lhop['nPoints'], error, color='blue', linestyle=lines[c], linewidth=2, marker='o', markersize=10, fillstyle='none', label='LHOP (%s)'%measure)
    for c, (measure, error) in enumerate(sorted(hat['rmse'].iteritems())):
        ax.plot(hat['nPoints'], error, color='red', linestyle=lines[c], linewidth=2, marker='o', markersize=10, fillstyle='none', label='Hat (%s)'%measure)
    ax.legend(loc="upper right", prop={'size':12})
    fig.tight_layout()
    fileName = os.path.join('plots', '%s-%sD.pdf'%(fileName, d))
    pdf = PdfPages(fileName)
    pdf.savefig(fig)
    pdf.close()
    plt.close(fig)

if __name__ == '__main__':
    main()
