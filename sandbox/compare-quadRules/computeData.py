from __future__ import division, absolute_import
import numpy as np
import json
import os
import sys

from dasg import Grid
from dasg.domain import hyper
from dasg.quadrule import LHOP_Gauss, Hat_Equidistant

sys.path.append(os.path.join(os.path.dirname(__file__), '../../', 'examples'))
from examples import Polynom, Genz, Poisson, Matrix


def main():
    compare_Polynom()
    compare_Genz()
    compare_FEM()
    compare_Matrix()

def compare_Polynom():
    nMC = 500
    tolerances = [1E-2, 1E-3, 1E-4, 1E-5]
    polynom = Polynom(hyper([-2, 1], 1))

    # LHOP
    lhop = {'volume': {'absolute': [], 'relative': []},
            'rmse':   {'absolute': [], 'relative': []},
            'nPoints': []}
    lhopRule = LHOP_Gauss(polynom)
    grid = Grid(polynom, tolerance=1, maxIter=500, quadRule=lhopRule)
    for tol in tolerances:
        grid.refine(tolerance=tol)
        rmse = grid.rmse(nMC)
        symbolicVol = polynom.symbolicVolume()

        lhop['volume']['absolute'].append(np.abs(symbolicVol - grid.integrate()[0]))
        lhop['volume']['relative'].append(np.abs((symbolicVol - grid.integrate()[0])/symbolicVol))
        for measure, error in rmse.iteritems():
            lhop['rmse'][measure].append(error)
        lhop['nPoints'].append(grid.info()['gridPoints'])

    # Hat
    hat = {'volume': {'absolute': [], 'relative': []},
           'rmse':   {'absolute': [], 'relative': []},
           'nPoints': []}
    hatRule = Hat_Equidistant(polynom)
    grid = Grid(polynom, tolerance=1, maxIter=500, quadRule=hatRule)
    for tol in tolerances: 
        grid.refine(tolerance=tol)
        rmse = grid.rmse(nMC)
        symbolicVol = polynom.symbolicVolume()

        hat['volume']['absolute'].append(np.abs(symbolicVol - grid.integrate()[0]))
        hat['volume']['relative'].append(np.abs((symbolicVol - grid.integrate()[0])/symbolicVol))
        for measure, error in rmse.iteritems():
            hat['rmse'][measure].append(error)
        hat['nPoints'].append(grid.info()['gridPoints'])

    storeData(polynom, 'Polynom', 'Polynom', lhop, hat)

def compare_Genz():
    types = ['productPeak', 'gaussian']
    dimensions = [8, 10]
    tolerances = [1E-2, 1E-3, 1E-4]

    for functionType, dimension in zip(types, dimensions):
        _compare_Genz(functionType, dimension, tolerances)

def _compare_Genz(functionType, dimension, tolerances):
    genz = Genz(dimension, functionType)
    nMC = 500

    # LHOP
    lhop = {'volume': {'absolute': [], 'relative': []},
            'rmse': {'absolute': [], 'relative': []},
            'nPoints': []}
    lhopRule = LHOP_Gauss(genz)
    grid = Grid(genz, tolerance=1, maxIter=500, quadRule=lhopRule)
    for tol in tolerances:
        grid.refine(tolerance=tol)
        rmse = grid.rmse(nMC)
        symbolicVol = genz.symbolicVolume()

        lhop['volume']['absolute'].append(np.abs(symbolicVol - grid.integrate()[0]))
        lhop['volume']['relative'].append(np.abs((symbolicVol - grid.integrate()[0])/symbolicVol))
        for measure, error in rmse.iteritems():
            lhop['rmse'][measure].append(error)
        lhop['nPoints'].append(grid.info()['gridPoints'])

    # Hat
    hat = {'volume': {'absolute': [], 'relative': []},
           'rmse': {'absolute': [], 'relative': []},
           'nPoints': []}
    hatRule = Hat_Equidistant(genz)
    grid = Grid(genz, tolerance=1, maxIter=500, quadRule=hatRule)
    for tol in tolerances:
        grid.refine(tolerance=tol)
        rmse = grid.rmse(nMC)
        symbolicVol = genz.symbolicVolume()

        hat['volume']['absolute'].append(np.abs(symbolicVol - grid.integrate()[0]))
        hat['volume']['relative'].append(np.abs((symbolicVol - grid.integrate()[0])/symbolicVol))
        for measure, error in rmse.iteritems():
            hat['rmse'][measure].append(error)
        hat['nPoints'].append(grid.info()['gridPoints'])

    storeData(genz, 'Genz (%s)'%functionType, 'Genz-%s'%functionType, lhop, hat)

def compare_FEM():
    nMC = 100
    tolerances = [5E-3, 5E-4, 5E-5]
    fem = Poisson(16)

    # LHOP
    lhop = {'rmse': {'absolute': [], 'relative': []},
            'nPoints': []}
    lhopRule = LHOP_Gauss(fem)
    grid = Grid(fem, tolerance=1, maxIter=500, quadRule=lhopRule)
    for tol in tolerances:
        grid.refine(tolerance=tol)
        rmse = grid.rmse(nMC)

        for measure, error in rmse.iteritems():
            lhop['rmse'][measure].append(error)
        lhop['nPoints'].append(grid.info()['gridPoints'])

    # Hat
    hat = {'rmse': {'absolute': [], 'relative': []},
           'nPoints': []}
    hatRule = Hat_Equidistant(fem)
    grid = Grid(fem, tolerance=1, maxIter=500, quadRule=hatRule)
    for tol in tolerances:
        grid.refine(tolerance=tol)
        rmse = grid.rmse(nMC)

        for measure, error in rmse.iteritems():
            hat['rmse'][measure].append(error)
        hat['nPoints'].append(grid.info()['gridPoints'])

    storeData(fem, 'Poisson SFEM', 'FEM', lhop, hat)

def compare_Matrix():
    nMC = 100
    tolerances = [1E-3, 1E-4, 1E-5]
    matrix = Matrix(9)

    # LHOP
    lhop = {'rmse': {'absolute': [], 'relative': []},
            'nPoints': []}
    lhopRule = LHOP_Gauss(matrix)
    grid = Grid(matrix, tolerance=1, maxIter=500, quadRule=lhopRule)
    for tol in tolerances:
        grid.refine(tolerance=tol)
        rmse = grid.rmse(nMC)

        for measure, error in rmse.iteritems():
            lhop['rmse'][measure].append(error)
        lhop['nPoints'].append(grid.info()['gridPoints'])

    # Hat
    hat = {'rmse': {'absolute': [], 'relative': []},
           'nPoints': []}
    hatRule = Hat_Equidistant(matrix)
    grid = Grid(matrix, tolerance=1, maxIter=500, quadRule=hatRule)
    for tol in tolerances:
        grid.refine(tolerance=tol)
        rmse = grid.rmse(nMC)

        for measure, error in rmse.iteritems():
            hat['rmse'][measure].append(error)
        hat['nPoints'].append(grid.info()['gridPoints'])

    storeData(matrix, 'Matrix function', 'Matrix', lhop, hat)

def storeData(function, title, fileName, lhop, hat):
    data = {'title': title,\
            'fileName': fileName,\
            'lhop': lhop,\
            'hat': hat,\
            'd': function.d}
    with open('data/%s-%sD.json'%(fileName, function.d), 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    main()
