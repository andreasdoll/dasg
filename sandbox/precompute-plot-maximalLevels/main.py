from __future__ import division, absolute_import
import numpy as np
import matplotlib.pyplot as plt

from dasg.grid.hashing.GridStructure import precompute_maximal_levels

def main():
    maximalLevelsJSON = precompute_maximal_levels()

    # prepare data 
    maximalLevels = {}
    for d, level in maximalLevelsJSON.iteritems():
        maximalLevels[int(d)] = int(level)

    dimensions = []
    levels = [] 
    for d, level in sorted(maximalLevels.iteritems()):
        dimensions.append(d)
        levels.append(level)

    dimensions = dimensions[:-2]
    levels = levels[:-2]

    # plot
    fig = plt.figure()
    fig.set_size_inches(7.5, 4.5)
    plt.rcParams.update({'font.size': 9})
    ax = fig.add_subplot(111)
    ax.set_title('Maximal overall refinment', fontsize=10)
    ax.bar(dimensions, levels, color='gray', align='center', width=1)
    ax.set_xticks(range(1, len(dimensions)+1))
    ax.set_xlabel('dimension')
    ax.set_ylabel('overall refinement level')
    ax.yaxis.grid()
    ax.set_yticks(np.arange(0, 160, 10))
    plt.xlim([0, len(dimensions)+1])
    plt.ylim([0, 150])
    fig.tight_layout()
    fig.savefig('maximalLevels.png')
    plt.close(fig)
    
    
if __name__ == '__main__':
    main()
