from __future__ import division, absolute_import
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure#, legend
import os
import sys

from dasg.quadrule import LHOP_Gauss, Hat_Equidistant
from dasg.domain import hyper

sys.path.append(os.path.join(os.path.dirname(__file__), '../../', 'examples'))

from examples import Polynom


def main():
    polynom = Polynom(hyper([0,1], 1))
    hat = Hat_Equidistant(polynom)

    polynom = Polynom(hyper([-1,1], 1))
    lhop = LHOP_Gauss(polynom)

    plot(hat, 4, "Hat, equidistant", 'Hat_Equidistant.png')
    plot(lhop, 4, "Local high order polynomials, gaussian", 'LHOP_Gauss.png')


def plot(quadRule, level, title, fileName):
    def isOdd(n):
        if n % 2 == 1:
            return True
        else: 
            return False

    colors = ['', 'blue', 'green', 'red', 'orange']

    fig = figure()
    fig.set_size_inches(6, 1.5)
    ax = fig.add_subplot(111)
    xs = np.linspace(quadRule.domain[0][0], quadRule.domain[0][1], 1000)

    for l in range(1, level+1):
        for helper, i in enumerate([n for n in range(1, np.power(2, l)) if isOdd(n)]):
            ys = np.zeros(xs.shape)
            for c, x in enumerate(xs):
                ys[c] = quadRule.evaluate([l], [i], [x])
            if helper == 0:
                ax.plot(xs, ys, ls='-', color=colors[l], label='level %i'%l)
            else:
                ax.plot(xs, ys, ls='-', color=colors[l])

    for l, pts in enumerate(quadRule.collPt[0][:level+1]):
        for pt in pts:
            plt.scatter(pt, 0.02, color=colors[l], marker='o', s=70) 

    plt.legend(loc='upper center', prop={'size':8}, ncol=helper)
    ax.set_xlim(quadRule.domain[0])
    ax.set_ylim([0,2])
    plt.rcParams.update({'font.size': 8})
    fig.tight_layout()
    fig.savefig(os.path.join('plots', fileName))


if __name__ == '__main__':
    main()
