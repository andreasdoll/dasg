from __future__ import division, absolute_import
import numpy as np
import json
import os
import sys

from dasg import Grid
from dasg.quadrule import LHOP_Gauss, Hat_Equidistant

sys.path.append(os.path.join(os.path.dirname(__file__), '../../', 'examples'))

from examples import Genz, Poisson, Matrix

def main():
    weights = [1, 0.99, 0.9]
    compare_Genz(weights)
    compare_FEM(weights)
    compare_Matrix(weights)

def compare_Genz(weights):
    types = ['productPeak', 'gaussian']
    dimensions = [8, 10]
    tolerances = [1E-2, 1E-3, 1E-4]

    for functionType, dimension in zip(types, dimensions):
        _compare_Genz(functionType, dimension, tolerances, weights)

def _compare_Genz(functionType, dimension, tolerances, weights):
    hat = {}
    lhop = {}

    nMC = 500
    genz = Genz(dimension, functionType)

    # LHOP
    lhopRule = LHOP_Gauss(genz)
    for weight in weights:
        lhop[weight] = {'volume': {'absolute': [], 'relative': []},
                        'rmse':   {'absolute': [], 'relative': []},
                        'nPoints': []}
        grid = Grid(genz, tolerance=1, adaptiveWeight=weight, maxIter=500, quadRule=lhopRule)
        for tol in tolerances:
            grid.refine(tolerance=tol)
            rmse = grid.rmse(nMC)
            symbolicVol = genz.symbolicVolume()

            lhop[weight]['volume']['absolute'].append(np.abs(symbolicVol - grid.integrate()[0]))
            lhop[weight]['volume']['relative'].append(np.abs((symbolicVol - grid.integrate()[0])/symbolicVol))
            for measure, error in rmse.iteritems():
                lhop[weight]['rmse'][measure].append(error)
            lhop[weight]['nPoints'].append(grid.info()['gridPoints'])


    # Hat
    hatRule = Hat_Equidistant(genz)
    for weight in weights:
        hat[weight] = {'volume': {'absolute': [], 'relative': []},
                       'rmse':   {'absolute': [], 'relative': []},
                       'nPoints': []}
        grid = Grid(genz, tolerance=1, adaptiveWeight=weight, maxIter=500, quadRule=hatRule)
        for tol in tolerances:
            grid.refine(tolerance=tol)
            rmse = grid.rmse(nMC)
            symbolicVol = genz.symbolicVolume()

            hat[weight]['volume']['absolute'].append(np.abs(symbolicVol - grid.integrate()[0]))
            hat[weight]['volume']['relative'].append(np.abs((symbolicVol - grid.integrate()[0])/symbolicVol))
            for measure, error in rmse.iteritems():
                hat[weight]['rmse'][measure].append(error)
            hat[weight]['nPoints'].append(grid.info()['gridPoints'])

    storeData(genz, 'Genz (%s)'%functionType, 'Genz-%s'%functionType, lhop, hat)

def compare_FEM(weights):
    hat = {}
    lhop = {}

    nMC = 100
    tolerances = [5E-3, 5E-4, 5E-5]
    
    fem = Poisson(16)

    # LHOP
    lhopRule = LHOP_Gauss(fem)
    for weight in weights:
        lhop[weight] = {'rmse': {'absolute': [], 'relative': []},
                        'nPoints': []}
        grid = Grid(fem, tolerance=1, adaptiveWeight=weight, maxIter=500, quadRule=lhopRule)
        for tol in tolerances:
            grid.refine(tolerance=tol)
            rmse = grid.rmse(nMC)

            for measure, error in rmse.iteritems():
                lhop[weight]['rmse'][measure].append(error)
            lhop[weight]['nPoints'].append(grid.info()['gridPoints'])

    # Hat
    hatRule = Hat_Equidistant(fem)
    for weight in weights:
        hat[weight] = {'rmse':   {'absolute': [], 'relative': []},
                       'nPoints': []}
        grid = Grid(fem, tolerance=1, adaptiveWeight=weight, maxIter=500, quadRule=hatRule)
        for tol in tolerances:
            grid.refine(tolerance=tol)
            rmse = grid.rmse(nMC)

            for measure, error in rmse.iteritems():
                hat[weight]['rmse'][measure].append(error)
            hat[weight]['nPoints'].append(grid.info()['gridPoints'])

    storeData(fem, 'Poisson SFEM', 'FEM', lhop, hat)

def compare_Matrix(weights):
    hat = {}
    lhop = {}

    nMC = 100
    tolerances = [1E-3, 1E-4, 1E-5]

    matrix = Matrix(9)

    # LHOP
    lhopRule = LHOP_Gauss(matrix)
    for weight in weights:
        lhop[weight] = {'rmse': {'absolute': [], 'relative': []},
                        'nPoints': []}
        grid = Grid(matrix, tolerance=1, adaptiveWeight=weight, maxIter=500, quadRule=lhopRule)
        for tol in tolerances:
            grid.refine(tolerance=tol)
            rmse = grid.rmse(nMC)

            for measure, error in rmse.iteritems():
                lhop[weight]['rmse'][measure].append(error)
            lhop[weight]['nPoints'].append(grid.info()['gridPoints'])

    # Hat
    hatRule = Hat_Equidistant(matrix)
    for weight in weights:
        hat[weight] = {'rmse': {'absolute': [], 'relative': []},
                        'nPoints': []}
        grid = Grid(matrix, tolerance=1, adaptiveWeight=weight, maxIter=500, quadRule=hatRule)
        for tol in tolerances:
            grid.refine(tolerance=tol)
            rmse = grid.rmse(nMC)

            for measure, error in rmse.iteritems():
                hat[weight]['rmse'][measure].append(error)
            hat[weight]['nPoints'].append(grid.info()['gridPoints'])

    storeData(matrix, 'Matrix function', 'Matrix', lhop, hat)

def storeData(function, title, fileName, lhop, hat):
    data = {'title': title,\
            'fileName': fileName,\
            'lhop': lhop,\
            'hat': hat,\
            'd': function.d}
    with open('data/%s-%sD.json'%(fileName, function.d), 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    main()
