from __future__ import division, absolute_import
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import os
import json

def main():
    if not os.path.exists('plots/'):
        os.makedirs('plots/')
    dataFiles = os.listdir('data/')
    for dataFile in dataFiles:
        data = readData('data/%s'%dataFile)
        if 'Polynom' in dataFile or 'Genz' in dataFile:
            plot(data)
        else:
            plot_without_volume(data)

def readData(fileName):
    with open(fileName, 'r') as f:
        content = json.load(f)
    return content

def plot(data):
    d = data['d']
    title = data['title']
    fileName = data['fileName']
    lhop = data['lhop']
    hat = data['hat']

    lines = ['-', '--', ':']
    fig = plt.figure()
    fig.set_size_inches(8.27, 11.7)
    ax = fig.add_subplot(211)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.title('%s (%sD): relative volume error'%(title, d))
    ax.set_xlabel("#gridpoints")
    ax.set_ylabel("relative volume error")
    for m, weight in enumerate(sorted(lhop.keys(), reverse=True)):
        ax.plot(lhop[weight]['nPoints'], lhop[weight]['volume']['relative'], color='blue', linestyle=lines[m], linewidth=2, marker='o', markersize=10, fillstyle='none', label='LHOP, w=%s'%weight)
        ax.plot(hat[weight]['nPoints'], hat[weight]['volume']['relative'], color='red', linestyle=lines[m], linewidth=2, marker='o', markersize=10, fillstyle='none', label='Hat, w=%s'%weight)
    ax.legend(loc="upper right", prop={'size':12})

    ax = fig.add_subplot(212)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.title('%s (%sD): relative RMSE'%(title, d))
    ax.set_xlabel("#gridpoints")
    ax.set_ylabel("relative RMSE")
    for m, weight in enumerate(sorted(lhop.keys(), reverse=True)):
        ax.plot(lhop[weight]['nPoints'], lhop[weight]['rmse']['relative'], color='blue', linestyle=lines[m], linewidth=2, marker='o', markersize=10, fillstyle='none', label='LHOP, w=%s'%weight)
        ax.plot(hat[weight]['nPoints'], hat[weight]['rmse']['relative'], color='red', linestyle=lines[m], linewidth=2, marker='o', markersize=10, fillstyle='none', label='Hat, w=%s'%weight)
    ax.legend(loc="upper right", prop={'size':12})
    fig.tight_layout()
    fileName = os.path.join('plots', '%s-%sD.pdf'%(fileName, d))
    pdf = PdfPages(fileName)
    pdf.savefig(fig)
    pdf.close()
    plt.close(fig)

def plot_without_volume(data):
    d = data['d']
    title = data['title']
    fileName = data['fileName']
    lhop = data['lhop']
    hat = data['hat']

    lines = ['-', '--', ':']
    fig = plt.figure()
    fig.set_size_inches(8.27, 11.7)

    ax = fig.add_subplot(111)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.title('%s (%sD): RMSE'%(title, d))
    ax.set_xlabel("#gridpoints")
    ax.set_ylabel("RMSE")
    for m, weight in enumerate(sorted(lhop.keys(), reverse=True)):
        ax.plot(lhop[weight]['nPoints'], lhop[weight]['rmse']['relative'], color='blue', linestyle=lines[m], linewidth=2, marker='o', markersize=10, fillstyle='none', label='LHOP, w=%s'%weight)
        ax.plot(hat[weight]['nPoints'], hat[weight]['rmse']['relative'], color='red', linestyle=lines[m], linewidth=2, marker='o', markersize=10, fillstyle='none', label='Hat, w=%s'%weight)
    ax.legend(loc="upper right", prop={'size':12})
    fig.tight_layout()
    fileName = os.path.join('plots', '%s-%sD.pdf'%(fileName, d))
    pdf = PdfPages(fileName)
    pdf.savefig(fig)
    pdf.close()
    plt.close(fig)

if __name__ == '__main__':
    main()
