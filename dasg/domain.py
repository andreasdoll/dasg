from __future__ import division, absolute_import
import logging

logger = logging.getLogger(__name__)

def hyper(support, d):
    domain = {}
    for j in range(d):
        domain[j] = support
    return domain

def transform_to_UnitDomain(domain, x):
    """ Transforms: x in [d,e] -> [0,1] """
    d, e = domain[0], domain[1]
    t = (x-d)/(e-d)
    return t

def transform_from_UnitDomain(domain, x):
    """ Transforms: x in [0,1] -> [d,e] """
    d, e = domain[0], domain[1]
    t = x*(e-d)+d
    return t

def transform_from_to(domain1, domain2, x):
    x = transform_to_UnitDomain(domain1, x)
    x = transform_from_UnitDomain(domain2, x)
    return x

def in_domain(point, domain):
    if len(point) != len(domain):
        logger.warning('Point has not the same dimensionality %s as domain %s', len(point), len(domain))
        return False
    for j, p in enumerate(point):
        if p < domain[j][0] or p > domain[j][1]:
            logger.warning('Point does not lie in domain in dimension %s', j)
            return False
    return True



# move to spgl
def supportsToDomain(supports):
    assert supports.shape[1] == 2
    domain = {}
    for j, support in enumerate(supports):
        domain[j] = list(support)
    return domain
