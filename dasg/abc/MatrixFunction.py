from abc import ABCMeta, abstractmethod, abstractproperty
import numpy as np

from dasg.abc import Function

class MatrixFunction(Function):
    """ Function with sparse matrix codomain to be integrated / interpolated """
    __metaclass__ = ABCMeta

    @abstractproperty
    def domain(self):
        """ Compact domain

            Returns
            -------
            Dictionary with
                keys: 0, ..., d
                values: np.arrays (2,) or 2-element lists
                        containing lower [0] and upper [1] domain boundaries 
        """
        raise NotImplementedError()

    @abstractproperty
    def dof(self):
        """ Dimensionality of the codomain (degrees of freedom)
        
            Returns
            -------
            int
                Dimensionality of the codomain
        """
        raise NotImplementedError()

    @abstractmethod
    def evaluate(self, x):
        """ Evaluation of a domain point.

            Parameters
            -------
            x: np.array (d,)
                Domain point to evaluate

            Returns
            -------
            np.array (dof,)
                Function evaluation of the domain point (sparse matrix data)
        """    
        raise NotImplementedError()

    @property
    def d(self):
        """ Dimensionality of the domain

            Returns
            -------
            int
                Dimensionality of the domain
        """
        return len(self.domain)

    @property
    def initialLevel(self):
        """ Initial non-vanishing level 

            Returns
            -------
            np.array (d,) 
                Norm of an codomain element 
        """
        return np.ones(self.d, dtype=int)

    def norm(self, value):
        """ User defined norm of the codomain 

            Returns
            -------
            float 
                Norm of an codomain element 
        """
        return np.linalg.norm(value, 2)

    @abstractproperty
    def CSR(self):
        """ Compressed sparse row information 

            Returns
            -------
            Dictionary holding CSR information
                'c': indices
                'r': indptr

            (compare scipy.sparse.csr_matrix)
        """
        raise NotImplementedError()
