from abc import ABCMeta, abstractmethod, abstractproperty

class QuadratureRule(object):
    """ A quadrature rule consists of
            - collocation points
            - hierarchical basis functions

        The class must implement hierarchical basis functions
        mapping onto reals, on given compact supports.
    """
    __metaclass__ = ABCMeta

    @abstractproperty
    def domain(self):
        """ Boundaries of the compact domain

            Returns
            -------
            Dict with 
                keys: 0, ..., d and
                values: 2-element lists/np.arrays holding
                        lower and upper boundaries in respective dimension 
        """
        raise NotImplementedError()

    @abstractproperty
    def collPt(self):
        """ Collocation points
            
            Returns
            -------
            Dict with 
                keys: 0, ..., d
                values: list of lists
                        each sublist holds the unique collocation points for 
                        levels 0, ..., maximal level per dimension.
        """

    @abstractmethod
    def volume(self, l, i):
        """ Volume of the basis function with level l and index i. 
            
            Parameters
            -------
            l: np.array (d,)
                multiindex of a level 

            i: np.array (d,)
                multiindex of an index

            Returns
            -------
            float
                Volume of the hierarchical basis function.
        """    
        raise NotImplementedError()

    @abstractmethod
    def evaluate(self, l, i, x):
        """ Evaluation of the basis function with level l
            and index i at a domain point x.
            
            Parameters
            -------
            l: np.array (d,)
                multiindex of a level 

            i: np.array (d,)
                multiindex of an index

            x: np.array (d,)
                domain point

            Returns
            -------
            float
                Evaluation of the basis function at x
        """    
        raise NotImplementedError()

    @property
    def d(self):
        """ Dimensionality of the quadrature rule 

            Returns
            -------
            int
                Dimensionality of the domain.
        """
        return len(self.domain)
