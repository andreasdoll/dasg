from __future__ import division, absolute_import
import logging

logger = logging.getLogger(__name__)

def work_per_level(l, diffGrids1D):
    """ Computes number of gridpoints neccessary 
        for evaluation of given level index 
    """
    d = len(l)
    nGridpoints = 1
    for j in range(d):
        nGridpoints *= len(diffGrids1D[l[j]])
    return nGridpoints

def compute_ErrorIndicator(l, lVol, initVol, adaptiveWeight, diffGrids1D, dof):
    """ Computes error indicator for given level.
    
        Returns
        -------
        float:
            Error indicator: unweighted error reduction

        float:
            Priority indicator: weighted parameter governing the heap sorting
    """
    if initVol != 0: 
        error = lVol/initVol
        weightedError = adaptiveWeight * error
        work = work_per_level(l, diffGrids1D[0])        # just the length is important
        weightedWork = (1-adaptiveWeight) * 1/work
        priorityIndicator = max(weightedError, weightedWork)
        return error, priorityIndicator
    else: 
        raise ZeroDivisionError('Can\'t compute error indicator: Initial differential integral vanishes')
