from __future__ import division, absolute_import
import heapq
import logging

from dasg.grid.adaptive.HeapObject import HeapObject

logger = logging.getLogger(__name__)

class ActiveSet(object):
    """ Heap of active indices """
    def __init__(self, key=lambda x:x):
        self.key = key
        self._data = []
        self._size = 0

    @property
    def size(self):
        return self._size

    @property
    def data(self):
        return self._data

    def increaseSize(self):
        self._size += 1

    def decreaseSize(self):
        self._size -= 1

    def add(self, l, lHash, error, priorityIndicator):
        """ Insert a tuple to heap """
        MI = HeapObject(l, lHash, error, priorityIndicator)
        heapq.heappush(self.data, MI)
        self.increaseSize()

    def remove(self):
        """ Remove tuple with highest priority indicator from heap """
        if self.size > 0:
            self.decreaseSize()
            MI = heapq.heappop(self.data)
            return MI.l, MI.lHash, MI.error
        else:
            logger.error('Can\'t pop heap - active index heap is empty. This shoudn\'t have happened!')
            raise LookupError('Can\'t pop heap - active index heap is empty. This shoudn\'t have happened!')
