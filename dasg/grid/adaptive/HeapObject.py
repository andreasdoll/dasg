from __future__ import division, absolute_import
import logging

logger = logging.getLogger(__name__)

class HeapObject(object):
    """ Tuple (levelindex, levelhash, error, priority indicator) """
    def __init__(self, level, lHash, error, priorityIndicator):
       self._l = level 
       self._lHash = lHash
       self._error = error
       self._priorityIndicator = priorityIndicator

    def __cmp__(self, other):
        """ Implements max-heap """
        return self.priorityIndicator < other.priorityIndicator

    @property
    def l(self):
        return self._l

    @property
    def lHash(self):
        return self._lHash

    @property
    def error(self):
        return self._error

    @property
    def priorityIndicator(self):
        return self._priorityIndicator
