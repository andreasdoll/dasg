from __future__ import division, absolute_import
import numpy as np
import logging

from dasg.grid.Grid import Grid

imported = {}
try:
    import scipy.sparse as sps
    imported['scipy'] = True
except ImportError:
    imported['scipy'] = False

logger = logging.getLogger(__name__)


class MatrixGrid(Grid):
    def __init__(self, \
                 function, \
                 tolerance, \
                 maxIter=100, \
                 maximalLevel=None, \
                 maximalLevelPerDimension=None, \
                 adaptiveWeight=None, \
                 nCPU=None, \
                 quadRule=None, \
                 logSettings=None):

    #def __init__(self, function, settings, quadRule=None, logSettings=None):
        assert imported['scipy'] == True, 'Cannot approximate matrix-valued function: scipy is not available'
        super(MatrixGrid, self).__init__(function, \
                                         tolerance, \
                                         maxIter, \
                                         maximalLevel, \
                                         maximalLevelPerDimension, \
                                         adaptiveWeight, \
                                         nCPU, \
                                         quadRule, \
                                         logSettings)


    def integrate(self):
        r = self.function.CSR['r']
        c = self.function.CSR['c']
        matrix_nonzero = Grid.integrate()
        matrix = sps.csr_matrix((matrix_nonzero, c, r), dtype=np.float_).toarray()
        return matrix

    def interpolate(self, x, returnMatrix=True):
        if returnMatrix == True:
            r = self.function.CSR['r']
            c = self.function.CSR['c']
            matrix_nonzero = super(MatrixGrid, self).interpolate(x)
            matrix = sps.csr_matrix((matrix_nonzero, c, r), dtype=np.float_).toarray()
            return matrix
        elif returnMatrix == False:
            matrix_nonzero = super(MatrixGrid, self).interpolate(x)
            return matrix_nonzero
        else:
            raise ValueError()
