from __future__ import division, absolute_import
import numpy as np
import copy
import logging
from Queue import Queue

from dasg.grid.adaptive.ActiveSet import ActiveSet
from dasg.grid.adaptive.utils import compute_ErrorIndicator
from dasg.grid.hashing.GridStructure import GridStructure 
from dasg.grid.hashing.HierarchisationStructure import HierarchisationStructure 
from dasg.grid.hashing.utils import precompute_hashes, levelHash
from dasg.grid.LevelWorker import LevelWorker
from dasg.grid.utils import unitVector, points_per_level, find_vanishing_facets, \
                            on_vanishing_facet, check_and_augment_settings
from dasg.grid.rmse import compute_rmse
from dasg.quadrule.lhop_gauss.LHOP_Gauss import LHOP_Gauss
from dasg.domain import in_domain
from dasg.logging.DataLogger import DataLogger

logger = logging.getLogger(__name__)


class Grid(object):
    """ Dimension adaptive sparse grid as in [GG03]. """
    def __init__(self, \
                 function, \
                 tolerance, \
                 maxIter=100, \
                 maximalLevel=None, \
                 maximalLevelPerDimension=None, \
                 adaptiveWeight=None, \
                 nCPU=None, \
                 quadRule=None, \
                 logSettings=None):

        self._settings = check_and_augment_settings(function, \
                                                    maximalLevel, \
                                                    maximalLevelPerDimension, \
                                                    adaptiveWeight, \
                                                    tolerance, \
                                                    maxIter, \
                                                    nCPU)
        self._function = function
        if quadRule == None:
            self._quadRule = LHOP_Gauss(function)
        else:
            self._quadRule = quadRule
        self._levelWorker = LevelWorker(self.settings['nCPU'])
        self._HS = GridStructure(self.d, \
                                 self.settings['maximalLevel'], \
                                 function.dof, \
                                 precompute_hashes(self.d, self.settings['maximalLevel']))
        self._error = 1
        self._iterCount = 0
        self._ActiveIndices = ActiveSet()
        self._initVolume = None
        self._info = None
        self._dataLogger = DataLogger(logSettings)
        self.dataLogger.storeSettings(self.settings)

        self._start_approximation()
        self.refine()

    @property
    def d(self):
        return self.function.d

    @property
    def function(self):
        return self._function

    @property
    def quadRule(self):
        return self._quadRule

    @property
    def settings(self):
        return self._settings

    @property
    def levelWorker(self):
        return self._levelWorker

    @property
    def HS(self):
        return self._HS

    @property
    def dataLogger(self):
        return self._dataLogger

    @property
    def iterCount(self):
        return self._iterCount

    def increase_iterCount(self):
        self._iterCount += 1

    @property
    def ActiveIndices(self):
        return self._ActiveIndices

    @property
    def error(self):
        return self._error

    def decrease_error(self, e):
        self._error -= e

    def increase_error(self, e):
        self._error += e

    @property 
    def initVolume(self):
        return self._initVolume


    # INTERFACE
    def info(self):
        return self._info

    def integrate(self):
        HS = self.HS
        dof = self.function.dof

        integral = np.zeros(dof)
        for lHash in range(HS.oLen):
            inner = HS.levels[lHash]
            if inner != None:
                for iHash in range(inner.rc):
                    integral += inner.hsp[iHash] * self.quadRule.volume(inner.l[iHash], inner.i[iHash]) 
        return integral

    def interpolate(self, x):
        if not in_domain(x, self.function.domain):
            logger.warning('Not interpolating point, it does not lie in the domain')
        else:
            dof = self.function.dof
            res = np.zeros(dof)
            for oHash in range(self.HS.oLen):
                inner = self.HS.levels[oHash]
                if inner != None:
                    for iHash in range(inner.rc):
                        res += inner.hsp[iHash] * self.quadRule.evaluate(inner.l[iHash], inner.i[iHash], x)
            return res

    def rmse(self, N):
        rmse = compute_rmse(self, N)
        return rmse

    def refine(self, tolerance=None, maxIter=None):
        if tolerance != None:
            if tolerance > self.settings['tol']:
                logger.warning('Chosen tolerance is larger than already chosen. Not setting new tolerance.')
            elif tolerance == self.settings['tol']:
                logger.warning('Chosen tolerance is equal to already chosen.')
            else:
                self.settings['tol'] = tolerance
        if maxIter != None:
            if maxIter < self.settings['maxIter']:
                logger.warning('Chosen maxIter is smaller than already chosen. Not setting new maxIter.')
            elif maxIter == self.settings['maxIter']:
                logger.warning('Chosen maxIter is equal to already chosen.')
            else:
                self.settings['maxIter'] = maxIter
        function = self.function
        dof = function.dof
        d = self.d
        HS = self.HS
        levelWorker = self.levelWorker
        adaptiveWeight = self.settings['adaptiveWeight']

        while not self._converged():
            self.increase_iterCount()
            activeLevel, lHash, error_perLevel = self.ActiveIndices.remove()
            HS.mark_asOld(lHash)
            self.decrease_error(error_perLevel)

            logger.info('Relative error = %s: %s active, selecting %s with error %s', "%6.2E"%(self.error+error_perLevel), self.ActiveIndices.size+1, activeLevel, "%6.2E"%error_perLevel)

            for j in range(d): 
                fw = activeLevel + unitVector(d, j)
                fwHash = levelHash(d, fw, HS.P)
                if self._isAdmissible(fw):
                    l, i, coord, rc = points_per_level(d, fw, self.quadRule.collPt)
                    l, i, coord, hsp = levelWorker.computeSurplus(self, fwHash, l, i, coord)
                    HS.insert(fwHash, l, i, coord, hsp, rc)
                    deltaFW = self._compute_differentialIntegral(l, i, hsp)
                    error_perLevel, priority_indicator = compute_ErrorIndicator(l[0, :], deltaFW, self.initVolume, adaptiveWeight, self.quadRule.collPt, dof)
                    self.ActiveIndices.add(l[0, :], fwHash, error_perLevel, priority_indicator)
                    self.increase_error(error_perLevel)

            self.dataLogger.storeIter(activeLevel, self.error, self.ActiveIndices.size, HS.nPoints, HS.maxRefinement)

        # prepare final info
        self._info = {'gridPoints': self.HS.nPoints,
                      'maximalRefinement': self.HS.maxRefinement,
                      'relativeError': self.error,
                      'flag': self._converged(returnFlags=True)}

        self.dataLogger._plot()


    # INTERNAL
    def _converged(self, returnFlags=False):
        ActiveIndices = self.ActiveIndices
        error = self.error
        if error > self.settings['tol']:
            if ActiveIndices.size == 0:
                if not returnFlags:
                    logger.warning('Failed to converge within given maximial level %s, consider increasing.', self.settings['level'])
                    return False
                else:
                    return 2
            elif ActiveIndices.size > 0:
                if self.iterCount < self.settings['maxIter']:
                    return False 
                else:
                    if not returnFlags:
                        logger.warning('Quadrature tolerance not met after %s steps, stopping.', self.settings['maxIter'])
                        logger.info('Generated %s grid points.', self.HS.nPoints)
                        return True
                    else:
                        return 1
        else:
            if not returnFlags:
                logger.info('Grid converged with tol=%s. Generated %s grid points.', "%6.2E"%error, self.HS.nPoints)
                return True
            else:
                return 0

    def _start_approximation(self):
        ################################
        # SETUP
        ################################
        d = self.d
        HS = self.HS
        levelWorker = self.levelWorker
        initialLevel = self.function.initialLevel
        HS.vanishingFacets = find_vanishing_facets(initialLevel)


        ################################
        # INIT STEP
        ################################
        initHash = levelHash(d, initialLevel, HS.P)
        
        l, i, coord, nPoints = points_per_level(d, initialLevel, self.quadRule.collPt)
        l, i, coord, hsp = levelWorker.computeSurplus(self, initHash, l, i, coord)
        HS.insert(initHash, l, i, coord, hsp, nPoints)
        self._initVolume = self._compute_differentialIntegral(l, i, hsp)
        if self.initVolume != 0:
            logger.info('Initial differential integral on level %s is %s', l[0, :], "%6.2E"%self.initVolume)
        else:
            logger.error('Initial differential integral on level %s vanishes.', l[0, :])
        error_perLevel = 1
        self.ActiveIndices.add(l[0, :], initHash, error_perLevel, error_perLevel)
        self.dataLogger.storeInit(initialLevel, self.initVolume)
        self.dataLogger.storeIter(initialLevel, self.error, self.ActiveIndices.size, HS.nPoints, HS.maxRefinement)

    def _isAdmissible(self, fwN):
        d = self.d
        HS = self.HS
        if d+self.settings['maximalLevel'] < sum(fwN):
            logger.warning('Forward neighbor %s exceeds overall maximal level %s', fwN, self.settings['maximalLevel'])
            return False
        for j in range(d):
            if fwN[j] > self.settings['maximalLevelPerDimension']:
                logger.warning('Forward neighbor %s exceeds maximal level per dimension %s', fwN, self.settings['maximalLevelPerDimension'])
                return False 
            if fwN[j] != 1:
                bwN = fwN - unitVector(d, j)
                bwNHash = levelHash(d, bwN, HS.P)
                if not on_vanishing_facet(bwN, HS.vanishingFacets):
                    if bwNHash >= HS.oLen:
                        return False
                    elif HS.levels[bwNHash] == None:
                        return False
                    elif HS.levels[bwNHash].isOld == False:
                        return False
        return True
 
    def _hierarchizeGP(self, l, i, x, val):
        """ Hierarchization of a gridpoint """
        knownFathers = HierarchisationStructure(self.d, self.HS.P)
        lowerLevelInterpol = self._traversePedigree(l, i, x, knownFathers)
        hsp = val - lowerLevelInterpol
        return hsp

    # TODO: Why is copy needed?! Get rid of this!
    def _traversePedigree(self, l, i, x, knownFathers):
        """ Traverses the pedigree of fathers up to the point on level 1 recursivly.
            Marks already met fathers and stops the path if father is already met. """
        def father(l, i, dim):
            """ Finds for given grid point a possibly existing father in specified dimension """
            tmpL = copy.deepcopy(l)
            tmpI = copy.deepcopy(i)
            if tmpL[dim] == 1:
                return None
            else:
                tmpL[dim] -= 1
                if tmpI[dim] != 1:
                    if np.mod(int((tmpI[dim]-1)/2), 2) == 1:
                        tmpI[dim] = int((tmpI[dim]-1)/2)
                    else:
                        tmpI[dim] = int((tmpI[dim]+1)/2)
                return tmpL, tmpI 

        HS = self.HS
        dof = self.function.dof

        res = np.zeros((1, dof), dtype=float)
        fatherQ = Queue()
        for dim in range(self.d):
            f = father(l, i, dim)
            if f is not None:
                if knownFathers.alreadyMet(f[0], f[1]) is False:
                    knownFathers.mark(f[0], f[1])
                    fatherQ.put(f)
   
        while not fatherQ.empty():
            f = fatherQ.get()
            if not on_vanishing_facet(f[0], HS.vanishingFacets):
                res += HS.lookupHSP_gp(f[0], f[1]) * self.quadRule.evaluate(f[0], f[1], x)
                grandfathers = self._traversePedigree(f[0], f[1], x, knownFathers)
                res += grandfathers
            else:
                res += 0    # father is on vanishing facet -> we are done here
        return res

 
    def _compute_differentialIntegral(self, l, i, hsp):
        deltaFW = 0
        for pt in range(l.shape[0]):
            deltaFW += hsp[pt, :] * self.quadRule.volume(l[pt, :], i[pt, :])
        return self.function.norm(deltaFW)
