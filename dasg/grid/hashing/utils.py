from __future__ import division, absolute_import
import numpy as np
import logging

logger = logging.getLogger(__name__)

def precompute_hashes(d, level):
    """ Precomputation step for hashing.
        G is not needed for the [Feu10] hashing variant """
    def calcP(k, m):
        res = np.uint64(0)
        for q in range(1, k+1):
            res += np.uint64(P[q, m-1])
        return res
    #def calcG(k, m):
    #    res = np.uint64(0)
    #    for q in range(k):
    #        res += np.uint64(np.power(2, q)*G[k-q, m-1])
    #    return res
    #logger.info('Precomputing hash values')
    n = d + level
    P = np.zeros((n+1, d), dtype=np.uint64)
    #G = np.zeros((n+1, d), dtype=np.uint64)
    for k in range(n+1):
        P[k, 0] = k
        #G[k, 0] = np.uint64(np.power(2, k) - 1)
    for m in range(1, d):
        for k in range(n+1):
            P[k, m] = calcP(k, m)
            #G[k, m] = calcG(k, m)
    return P

def levelHash(dim, l, P):
    """ Unique hash for a level multiindex l. """
    minLevel = 1
    d = 0 
    n = 1
    r = 0
    for m in range(dim-1, -1, -1):
        d += 1
        n += l[m] - minLevel 
        r += P[n-1, d-1]
    return np.int(r)

def indexHash(l, i):
    """ Inner hash for index multiindices i. """
    d = len(l)
    res = 0
    for j in range(d):
        prod = np.product([np.power(2, l[k]-1) for k in range(j)])
        res += int(np.floor(i[j]/2))*prod
    return int(res)
