from __future__ import division, absolute_import
import numpy as np
import logging
import os
import json
import warnings

from dasg.grid.hashing.utils import levelHash, indexHash, precompute_hashes

logger = logging.getLogger(__name__)

class GridStructure(object):
    """ Container structure for storage:(outer) level-buckets """
    def __init__(self, d, maxLevel, dof, P):
        self.d = d
        self.level = maxLevel
        self.levels = []
        self.oLen = 0
        self.dof = dof
        self.P = P
        self._maxRefinement = np.zeros(d, dtype=int)
        self._nPoints = 0

    @property
    def maxRefinement(self):
        return self._maxRefinement

    @property
    def vanishingFacets(self):
        return self._vanishingFacets

    @vanishingFacets.setter
    def vanishingFacets(self, vF):
        self._vanishingFacets = vF

    def insert(self, lHash, l, i, coord, hsp, rc, val=None):
        """ Interface to insert to hash structure """
        d = self.d
        dof = self.dof
        if lHash < self.oLen:
            if self.levels[lHash] is None:
                self.levels[lHash] = InnerGridStructure(d, dof, l, i, coord, hsp, rc, val)
            else:
                logger.error('Data already set. This shouldn\'t have happened')
                logger.error('happens at %s, %s', l[0,:], lHash)
        elif lHash == self.oLen:
            self.levels.append(InnerGridStructure(d, dof, l, i, coord, hsp, rc, val))
            self.oLen += 1
        else:
            gap = lHash - self.oLen 
            for j in range(gap):
                self.levels.append(None)
            self.levels.append(InnerGridStructure(d, dof, l, i, coord, hsp, rc, val))
            self.oLen += gap + 1

        self.updateMaxRefinement(l[0, :])
        self.increastPoints(rc)

    def increastPoints(self, N):
        self._nPoints += N

    @property
    def nPoints(self):
        return self._nPoints

    def updateMaxRefinement(self, level):
        for j, refinement in enumerate(level):
            if self.maxRefinement[j] < refinement:
                self._maxRefinement[j] = refinement

    def lookupHSP_gp(self, l, i):
        """ Returns hsp of gridpoint """
        d = self.d
        P = self.P
        lHash = levelHash(d, l, P)
        iHash = indexHash(l, i)
        return self.levels[lHash].hsp[iHash, :]   # hsp 

    def lookupHSP_level(self, lHash, info):
        """ Returns hsp of level """
        HS = self.HS
        return HS.levels[lHash].hsp, HS.levels[lHash].rc     # hsp, rc

    def mark_asOld(self, lHash):
        """ Mark a level as old index """
        self.levels[lHash].isOld = True 

    def retrieve(self, lHash):
        if self.oLen <= lHash:
            return None
        elif self.levels[lHash] == None:
            return None
        else:
            level = self.levels[lHash]
            return level.l, level.i, level.coord, level.val, level.rc

class InnerGridStructure(object):
    """Container structure for storage: (inner) indexhashes"""
    def __init__(self, d, dof, l, i, coord, hsp, rc, val=None):
        self.d = d
        self.l = l
        self.i = i
        self.coord = coord
        self.hsp = hsp
        self.rc = rc      # points per level
        self.isOld = False
        if val != None:
            self.val = val


def precompute_maximal_levels():
    path = os.path.dirname(__file__)
    dataFile = os.path.join(path, 'maximalLevels.json')

    maximalLevels = _compute_maximal_levels()

    with open(dataFile, 'w') as f:
        json.dump(maximalLevels, f)

    return maximalLevels

def _compute_maximal_levels():
    maximalLevels = {}
    maximalLevels[35] = 0
    for d in range(34, 0, -1):
        maximalLevels[d] = _compute_maximal_level_per_dimension(d, maximalLevels[d+1])
    return maximalLevels

def _compute_maximal_level_per_dimension(d, minimalCandidate):
    dof = 1
    for level in range(minimalCandidate, d*11+1):
        try:
            warnings.filterwarnings('error')
            np.seterr(all='warn')
            _ = GridStructure(d, level, dof, precompute_hashes(d, level))
        except Warning:
            return level-1
    return d*11
