from __future__ import division, absolute_import
import numpy as np
import logging

from dasg.grid.hashing.utils import levelHash, indexHash

logger = logging.getLogger(__name__)

class HierarchisationStructure(object):
    """Outer father structure for hierarchization to mark already met fathers"""
    def __init__(self, d, P):
        self.d = d
        self.lBin = []
        self.oLen = 0
        self.P = P

    def mark(self, l, i):
        pos = levelHash(self.d, l, self.P)
        if pos < self.oLen:
            if self.lBin[pos] is not None:
                self.lBin[pos].mark(l, i)
            else:
                self.lBin[pos] = InnerHierarchisationStructure(self.d)
                self.lBin[pos].mark(l, i)
        elif pos == self.oLen:
            self.lBin.append(InnerHierarchisationStructure(self.d))
            self.lBin[pos].mark(l, i)
            self.oLen += 1
        else:
            gap = pos - self.oLen
            for j in range(gap):
                self.lBin.append(InnerHierarchisationStructure(self.d))
            self.lBin.append(InnerHierarchisationStructure(self.d))
            self.lBin[pos].mark(l, i)
            self.oLen += gap + 1

    def alreadyMet(self, l, i):
        lHash = levelHash(self.d, l, self.P)
        iHash = indexHash(l, i)
        if lHash >= self.oLen:
            return False
        else:
            if iHash >= self.lBin[lHash].iLen:
                return False
            else:
                return self.lBin[lHash].iBin[iHash]

class InnerHierarchisationStructure(object):
    """Inner father structure for hierarchization to mark already met fathers"""
    def __init__(self, d):
        self.d = d
        self.iBin = np.zeros(1, dtype=bool)
        self.iLen = 0

    def mark(self, l, i):
        pos = indexHash(l, i)
        if pos < self.iLen:
            self.iBin[pos] = True 
        elif pos == self.iLen:
            if pos is 0:
                self.iBin[pos] = True
                self.iLen += 1
            else:
                self.iBin = np.append(self.iBin, [True], axis=0)
                self.iLen += 1
        else:
            gap = pos - self.iLen + 1
            tmp = np.zeros(gap, dtype=float)
            tmp[gap-1] = True
            self.iBin = np.append(self.iBin, tmp, axis=0)
            self.iLen += gap
