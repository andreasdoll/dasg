from __future__ import division, absolute_import
import numpy as np
import gc
import logging

from dasg.domain import transform_from_to
from dasg.grid.utils import instance

imported = {}
try:
    from joblib import Parallel, delayed
    imported['joblib'] = True
except ImportError:
    imported['joblib'] = False

logger = logging.getLogger(__name__)

_SPARSE_GRID = None

def compute_rmse(grid, N):
    global _SPARSE_GRID
    _SPARSE_GRID = grid

    logger.info('Starting RMSE for %s MC points', N)
    function = grid.function
    dof = function.dof
    nCPU = grid.levelWorker.nCPU

    MCpoints = create_MCpoints(N, function.d, function.domain)

    if nCPU == 1:
        output = np.zeros((N, dof*2))
        for pt in range(N):
            output[pt, :] = _workOnMCpoint(MCpoints[pt])

    elif nCPU > 1:
        output = np.array(Parallel(nCPU)(delayed(_workOnMCpoint)
                         (MCpoints[pt]) \
                         for pt in range(N)))

    rmse = {'absolute': 0,
            'relative': 0} 
    for pt in range(N):
        det  = output[pt, 0:dof]
        coll = output[pt, dof:2*dof]
        diffNorm = function.norm(det-coll)
        rmse['absolute'] += np.power(diffNorm, 2)
        rmse['relative'] += np.power(diffNorm, 2) / np.power(function.norm(det), 2)
    rmse['absolute'] = np.sqrt(rmse['absolute']/N)
    rmse['relative'] = np.sqrt(rmse['relative']/N)

    logger.info('RMSE finished with error %s (relative), %s (absolute)', "%6.2E"%rmse['relative'], "%6.2E"%rmse['absolute'])
    return rmse

def _workOnMCpoint(coord):
    grid = _SPARSE_GRID
    
    deterministic = grid.function.evaluate(coord)
    if instance(grid) == 'Grid':
        collocation = grid.interpolate(coord)
    elif instance(grid) == 'MatrixGrid':
        collocation = grid.interpolate(coord, returnMatrix=False)

    gc.collect()
    return np.concatenate([deterministic, collocation])

def create_MCpoints(nMC, d, domain):
    MCpointsUnit = np.random.rand(nMC, d) 
    MCpoints = np.zeros((nMC, d))
    for j in range(d):
        for p in range(nMC):
            MCpoints[p, j] = transform_from_to([0,1], domain[j], MCpointsUnit[p, j])
    return MCpoints
