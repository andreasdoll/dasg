from __future__ import division, absolute_import
import numpy as np
import gc
import logging

from dasg.grid.hashing.utils import indexHash

imported = {}
try:
    from joblib import Parallel, delayed
    imported['joblib'] = True
except ImportError:
    imported['joblib'] = False


_SPARSE_GRID = None

logger = logging.getLogger(__name__)

class LevelWorker(object):
    def __init__(self, nCPU):
        self._nCPU = nCPU

    @property
    def nCPU(self):
        return self._nCPU

    def computeSurplus(self, grid, lHash, l, i, coord):
        global _SPARSE_GRID
        _SPARSE_GRID = grid

        d = grid.d
        dof = grid.function.dof
        nGP = l.shape[0]

        if self.nCPU == 1:
            output = np.zeros((nGP, 2+3*d+dof))
            for pt in range(nGP):
                output[pt, :] = _workOnGridPoint(lHash, l[pt, :], i[pt, :], coord[pt, :])

        elif self.nCPU > 1:
            output = np.array(Parallel(self.nCPU)(delayed(_workOnGridPoint)\
                             (lHash, l[pt, :], i[pt, :], coord[pt, :])\
                             for pt in range(nGP)))

        return _sort_and_split_data(output, grid.d, grid.function.dof, nGP)      



def _workOnGridPoint(lHash, l, i, coord):
    grid = _SPARSE_GRID
    function = grid.function

    val = function.evaluate(coord)
    hsp = grid._hierarchizeGP(l, i, coord, val)
    iHash = indexHash(l, i)

    gc.collect()
    return np.concatenate([[lHash], [iHash], l, i, coord, hsp[0]], axis = 0)


def _sort_and_split_data(fw_Data, d, dof, nGP):
    sorted_array = _sortByIndexHash(fw_Data, d, dof, nGP)

    #lHash   = sorted_array[0, 0]
    l       = sorted_array[:, 2:2+d]
    i       = sorted_array[:, 2+d:2+2*d]
    coord   = sorted_array[:, 2+2*d:2+3*d]
    hsp     = sorted_array[:, 2+3*d:2+3*d+dof]

    return l.astype(int), i.astype(int), coord, hsp


def _sortByIndexHash(D, d, dof, nGP):
    sorted_array = np.zeros((nGP, 2+3*d+dof))

    for gp in D:
        iHash = gp[1]
        sorted_array[int(iHash), :] = gp
    
    return sorted_array
