from __future__ import division, absolute_import
import logging
import numpy as np
import itertools
import os
import json
from multiprocessing import cpu_count

logger = logging.getLogger(__name__)

imported = {}
try:
    from joblib import Parallel, delayed
    imported['joblib'] = True
except ImportError:
    imported['joblib'] = False

def find_vanishing_facets(initialLevel):
    vanishingFacets = []
    for j, level in enumerate(initialLevel):
        if level != 1:
            vanishingFacets.append(j)
    return vanishingFacets

def on_vanishing_facet(fwN, vanishingFacets):
    for f in vanishingFacets:
        if fwN[f] == 1:
            return True
    return False

def points_per_level(d, mi, coord1Ds):
    """ Calculates for a given level-multiindex all admissible points.
        That is: indexset and associated coordinates. 
    """
    def indexSet1D(l):
        """ 1D index sets of (odd) basis functions """
        if l == 0:
            return [0, 1]
        else:
            return [i for i in range(1, int(np.power(2, l))) if i % 2]

    indexSets = map(lambda n: indexSet1D(n), mi)
    isCombinations = []
    for elem in itertools.product(*indexSets):
        isCombinations.append(elem)
    rc = len(isCombinations)
    l = np.zeros((rc, d), dtype=int)
    i = np.zeros((rc, d), dtype=int)
    coord = np.zeros((rc, d), dtype=float)
    for j in range(rc):
        l[j, :] = mi
        i[j, :] = isCombinations[j]
        for k in range(d):
            coord[j, k] = lookup(mi[k], i[j, k], coord1Ds[k])
    return l, i, coord, rc

def unitVector(d, j):
    """ Unit vector """
    v = np.zeros(d, dtype=int)
    v[j] = 1
    return v

def lookup(l, i, data):
    """Translates odd basis numbering to even equivalent for lookups."""
    el = int(np.floor(i/2))
    return data[l][el] 

def check_and_augment_settings(function, \
                               maximalLevel, \
                               maximalLevelPerDimension, \
                               adaptiveWeight,\
                               tolerance, \
                               maxIter, \
                               nCPU):
    # check function dimensionality
    d = function.d
    if d < 1:
        logger.error('Invalid dimensionality of domain, must be > 0')
        raise ValueError('Invalid dimensionality of domain, must be > 0')
    elif d >= 34:
        logger.error('Invalid dimensionality of domain, must be < 34')
        raise ValueError('Invalid dimensionality of domain, must be < 34')

    # check initial level
    for j, level in enumerate(function.initialLevel):
        if level < 1:
            logger.error('Invalid initial level %s in dimension %s, must be > 0', level, j)
            raise ValueError('Invalid initial level %s in dimension %s, must be > 0', level, j)
        if level > 11:
            logger.error('Invalid initial level %s in dimension %s, must be < 12', level, j)
            raise ValueError('Invalid initial level %s in dimension %s, must be < 12', level, j)

    # load precomputed maximal levels
    path = os.path.dirname(__file__) 
    maximalLevelsFile = os.path.join(path, 'hashing/maximalLevels.json')
    with open(maximalLevelsFile, 'r') as f:
        maximalLevels = json.load(f)

    # check overall maximal level
    requiredMaximalLevel = np.sum(function.initialLevel) - d
    maximalPossibleLevel = maximalLevels[str(d)]
    if maximalLevel == None: 
        maximalLevel = maximalPossibleLevel
    else:
        if maximalLevel < requiredMaximalLevel:
            logger.warning('Chosen maximal level %s is too low. Using maximum %s', maximalLevel, maximalPossibleLevel)
            maximalLevel = maximalPossibleLevel
        elif maximalLevel > maximalPossibleLevel:
            logger.warning('Chosen maximal level %s it too large. Using maximum %s', maximalLevel, maximalPossibleLevel)
            maximalLevel = maximalPossibleLevel


    # check maximal level per dimension
    requiredMaximalLevelPerDimension = np.max(function.initialLevel)
    if maximalLevelPerDimension == None:
        maximalLevelPerDimension = 11
    else:
        if maximalLevelPerDimension < requiredMaximalLevelPerDimension:
            logger.warning('Chosen maximal level per dimension %s it too low. Using maximum 11', maximalLevelPerDimension)
            maximalLevelPerDimension = 11
        elif maximalLevelPerDimension > 11:
            logger.warning('Chosen maximal level per dimension %s it too large. Using maximum 11', maximalLevelPerDimension)
            maximalLevelPerDimension = 11

    # check adaptive weight
    if adaptiveWeight == None: 
        adaptiveWeight = 1
    else:
        if adaptiveWeight < 0:
            logger.warning('Invalid adaptive weight %s, must be float in [0, 1]. Using default 1', adaptiveWeight)
            adaptiveWeight = 1
        elif adaptiveWeight > 1:
            logger.warning('Invalid adaptive weight %s, must be float in [0, 1]. Using default 1', adaptiveWeight)
            adaptiveWeight = 1

    # check maximal iterations
    if maxIter < 1:
        logger.warning('Invalid number of allowed iterations %s, setting to 100', maxIter)
        maxIter = 100

    # check number of CPU cores
    if imported['joblib']:
        if nCPU == None: 
            nCPU = cpu_count()
        elif nCPU not in range(1, cpu_count()+1):
            logger.warning('Invalid number of CPU cores. Using all available (%s)', cpu_count())
            nCPU = cpu_count()
    else:
        if nCPU != None: 
            logger.warning('Cannot import joblib. Ignoring #CPU, performing sequential')
            nCPU = 1

    return {'maximalLevel': maximalLevel,
            'maximalLevelPerDimension': maximalLevelPerDimension,
            'adaptiveWeight': adaptiveWeight,
            'tol': tolerance,
            'maxIter': maxIter,
            'nCPU': nCPU}


def instance(c):
    return str(type(c)).split('\'')[1].split('.')[3]
