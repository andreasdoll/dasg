from __future__ import division, absolute_import
import logging.config
import os
from dasg.logging.utils import augment_text_settings

def setup_text_logger(settings={}):
    settings = augment_text_settings(settings)
    outDir = settings['outDir']
    textFile = settings['textFile']

    if not os.path.exists(outDir):
        os.makedirs(outDir)

    logger = logging.getLogger(__name__)

    logging.config.fileConfig('%s/logging.conf'%os.path.dirname(__file__), 
                              disable_existing_loggers=False, 
                              defaults={'logfilename': os.path.join(outDir, textFile)})

    return logger
