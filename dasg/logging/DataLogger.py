from __future__ import division, absolute_import
import numpy as np
import logging
import logging.config
import os
import json

from dasg.logging.utils import arr2list, augment_plot_settings

imported = {}
try:
    import matplotlib.pyplot as plt
    from matplotlib.backends.backend_pdf import PdfPages
    imported['matplotlib'] = True
except ImportError:
    imported['matplotlib'] = False


logger = logging.getLogger(__name__)


class DataLogger(object):
    def __init__(self, settings):
        if settings == True:
            settings = {}
        if type(settings) == dict:
            self._active = True
            self._settings = augment_plot_settings(settings, imported['matplotlib'])
            self._setup()
        else:
            self._active = False

    @property
    def active(self):
        return self._active

    @property
    def settings(self):
        return self._settings

    @property
    def dataFile(self):
        outDir = self.settings['outDir']
        dataFile = self.settings['dataFile']
        return os.path.join(outDir, dataFile)
        
    def storeSettings(self, settings):
        if self.active:
            self._appendToFile(settings)

    def storeInit(self, level, volume):
        if self.active:
            data = {'level': arr2list(level),
                    'volume': volume}
            self._appendToFile(data)

    def storeIter(self, level, error, heapSize, gridPoints, maxRefinement):
        if self.active:
            data = {'active': arr2list(level),
                    'error': error,
                    'heapSize': heapSize,
                    'gridpoints': gridPoints,
                    'maxRefinement': arr2list(maxRefinement)}
            self._appendToFile(data)
            if self.settings['livePlot']:
                self._plot()

    def _setup(self):
        outDir = self.settings['outDir']
    
        if not os.path.exists(outDir):
            os.makedirs(outDir)
    
        with open(self.dataFile, 'w') as f:
            json.dump([], f)

    def _appendToFile(self, data):
        log = self._readFromFile()
        log.append(data)
        with open(self.dataFile, 'w') as f:
            json.dump(log, f)
    
    def _readFromFile(self):
        with open(self.dataFile, 'r') as f:
            content = json.load(f)
        return content

    def _plot(self):
        if self.active: 
            log = self._readFromFile()

            #init = log[1]
            gridpoints = [] 
            error = [] 
            heapSize = []
            maxRefinement = []
            for l in log[2:]:
                gridpoints.append(l['gridpoints'])
                error.append(l['error'])
                heapSize.append(l['heapSize'])
                maxRefinement.append(l['maxRefinement'])

            iterations = range(1, len(error)+1)

            # DEFINE PLOT
            fig = plt.figure()
            plt.rcParams.update({'font.size': 12})
            fig.set_size_inches(8.27, 11.7)


            # PLOT 1
            ax = fig.add_subplot(3,1,1)
            ax.set_title('Convergence per work', fontsize=14)
            ax.plot(gridpoints, error, label='relative error reduction', marker='o', linewidth=3, markersize=8, color='blue')
            ax.plot()
            ax.set_xscale('log')
            ax.set_yscale('log')
            ax.set_xlabel('gridpoints')
            ax.set_ylabel('relative error')
            ax.yaxis.label.set_color('blue')
            ax.grid()


            # PLOT 2
            ax = fig.add_subplot(3,1,2)
            ax2 = ax.twinx()
            ax.set_title('Convergence per iteration', fontsize=14)
            ax.plot(iterations, error, label='relative error reduction', marker='o', linewidth=3, markersize=8, color='blue')
            ax2.plot(iterations, heapSize, label='active indices', marker='o', linewidth=3, markersize=8, color='red')
            ax.set_yscale('log')
            ax.set_xlabel('iterations')
            ax.set_ylabel('relative error')
            ax2.set_ylabel('active indices')
            ax.yaxis.label.set_color('blue')
            ax2.yaxis.label.set_color('red')
            ax.grid()
            plt.xlim(1, len(iterations)+1)
            #ax.annotate("Initial volume: %s on %s"%(init['volume'], init['level']), (1, error[0]))


            # PLOT 3
            maxRefinement = maxRefinement[-1]
            dimensions = range(1, len(maxRefinement)+1)
            ax = fig.add_subplot(3,1,3)
            ax.set_title('Maximal refinment per dimension', fontsize=14)
            ax.bar(dimensions, maxRefinement, color='gray', align='center', width=1/3)
            ax.set_xticks(dimensions)
            ax.set_xlabel('dimension')
            ax.set_yticks(range(int(max(maxRefinement)+2)))
            ax.set_ylabel('refinement level')
            plt.ylim([1, np.max(maxRefinement)+1])


            # FINISH PLOT
            fig.tight_layout()
            outDir = self.settings['outDir']
            plotFile = self.settings['plotFile']
            fileName = os.path.join(outDir, plotFile)
            pdf = PdfPages(fileName)
            pdf.savefig(fig)
            pdf.close()
            plt.close(fig)
