from __future__ import division, absolute_import
import logging

logger = logging.getLogger(__name__)

def arr2list(array):
    return array.tolist()

def augment_text_settings(settings):
    if 'outDir' not in settings.keys():
        settings['outDir'] = 'log'
    if 'textFile' not in settings.keys():
        settings['textFile'] = 'text.log'
    return settings

def augment_plot_settings(settings, imported_matplotlib):
    if 'outDir' not in settings.keys():
        settings['outDir'] = 'log'
    if 'dataFile' not in settings.keys():
        settings['dataFile'] = 'data.json'
    if imported_matplotlib:
        if 'plotFile' not in settings.keys():
            settings['plotFile'] = 'data.pdf'
        if 'livePlot' not in settings.keys():
            settings['livePlot'] = False 
    else:
        logger.warning('Not plotting anything: matplotlib is not available!')
    return settings
