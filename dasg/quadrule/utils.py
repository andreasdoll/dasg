from __future__ import division, absolute_import

from dasg.domain import transform_from_to

def resizeFactors(defaultDomain, domains, d):
    """ Find resize factor to account for domain resize """
    resizeFactors = []
    for j in range(d):
        a = transform_from_to(defaultDomain, domains[j], defaultDomain[0])
        b = transform_from_to(defaultDomain, domains[j], defaultDomain[1])
        diff = b - a
        org = defaultDomain[1] - defaultDomain[0]
        factor = diff/org
        resizeFactors.append(factor)
    return resizeFactors
