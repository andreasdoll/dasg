from __future__ import division
import numpy as np
import logging
from itertools import combinations

imported = {}
try:
    from sympy import symbols, Poly, integrate
    imported['sympy'] = True
except ImportError:
    imported['sympy'] = False

logger = logging.getLogger(__name__)

def find_lower_roots(roots_perLevel, basisSupports, support):
    roots = []
    for l in range(len(roots_perLevel)):
        for b in range(len(basisSupports[l])):
            if in_support(support, basisSupports[l][b]):
                #logger.debug('  appending %s', roots_perLevel[l])
                roots.append(roots_perLevel[l][b])
    return roots

def in_support(support, old_support):
    a, b = support[0], support[1]
    b1, b2 = old_support[0], old_support[1]
    if a >= b1 and a <= b2 and \
       b >= b1 and b <= b2:
        return True
    else:
        return False

def integrate_basisProduct(support, coeffs):
    coeffs = [1]+coeffs
    x = symbols('x')
    polynom = Poly(coeffs, x).as_expr()
    res = integrate(polynom, (x, support[0], support[1]))
    return res

def vieta_coeffitients(roots):
    roots = map(lambda r: -r, roots)
    nRoots = len(roots)
    coeffs = []
    for j in range(1, nRoots+1):
        tmp = list(combinations(roots, j))
        tmp2 = map(np.product, tmp)
        coeffs.append(np.sum(tmp2))
    return coeffs

def create_new_supports(roots, supports):
    boundaries = sorted(roots+supports)
    supports_perLevel = []
    for i in range(len(boundaries)-1):
        supports_perLevel.append(boundaries[i:i+2])
    return supports_perLevel

def compute_point(roots, support):
    coeffs = vieta_coeffitients(roots)
    numerator = _integrate_numerator(support, coeffs)
    denominator = integrate_basisProduct(support, coeffs)
    x = numerator/denominator
    return x

def _integrate_numerator(support, coeffs):
    coeffs = [1]+coeffs+[0]
    x = symbols('x')
    polynom = Poly(coeffs, x).as_expr()
    res = integrate(polynom, (x, support[0], support[1]))
    return res
