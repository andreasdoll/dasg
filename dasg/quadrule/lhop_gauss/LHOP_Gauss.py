from __future__ import division, absolute_import
import logging
from functools import partial
import os
import json

from dasg.abc.QuadratureRule import QuadratureRule
from dasg.domain import transform_from_to
from dasg.quadrule.utils import resizeFactors
from dasg.quadrule.lhop_gauss.utils import find_lower_roots, integrate_basisProduct, vieta_coeffitients
from dasg.grid.utils import lookup

logger = logging.getLogger(__name__)

class LHOP_Gauss(QuadratureRule):
    """ Implements local high order polynomials
        on gaussian collocation points [Dir00].
    """
    def __init__(self, function, maxLevel=11):
        self._defaultDomain = [-1, 1]       # NEVER modify!
        self._domain = function.domain
        self._collPt, self._basisSupports = self._setData(maxLevel)
        self._resizeFactors = resizeFactors(self.defaultDomain, function.domain, function.d) 

    @property
    def defaultDomain(self):
        return self._defaultDomain

    @property
    def domain(self):
        return self._domain

    @property
    def collPt(self):
        return self._collPt

    @property
    def basisSupports(self):
        return self._basisSupports

    @property
    def resizeFactors(self):
        return self._resizeFactors

    def volume(self, l, i):
        result = 1
        for j in range(self.d):
            result *= self._volume1D(l[j], i[j], j)
        return float(result)

    def evaluate(self, l, i, x):
        result = 1
        for j in range(self.d):
            result *= self._evaluate_on_1D(l[j], i[j], x[j], j)
        return float(result)

    def _volume1D(self, l, i, dim):
        support = lookup(l, i, self.basisSupports[dim])

        roots = find_lower_roots(self.collPt[dim], self.basisSupports[dim], support)

        root_onLevel = lookup(l, i, self.collPt[dim])
        roots = list(set(roots) - set([root_onLevel]))

        coeffs = vieta_coeffitients(roots)

        numerator = integrate_basisProduct(support, coeffs)
        denominator = 1
        for r in roots:
            denominator *= (root_onLevel - r)
        return float(numerator/denominator)

    def _evaluate_on_1D(self, l, i, x, dim):
        support = lookup(l, i, self.basisSupports[dim])
        if x >= support[0] and x <= support[1]:
            roots = find_lower_roots(self.collPt[dim], self.basisSupports[dim], support)
            root_onLevel = lookup(l, i, self.collPt[dim])
            roots = list(set(roots) - set([root_onLevel]))

            numerator = 1
            denominator = 1
            for r in roots:
                numerator *= (x - r)
                denominator *= (root_onLevel - r)
            return numerator/denominator
        else: 
            return 0

    def _setData(self, level):
        allRoots, allSupports = self._loadData_fromHD()

        if level < len(allRoots) - 1:
            logger.info('Gauss data up to level %s is available!', len(allRoots)-1)
            allRoots = allRoots[:level+1]
            allSupports = allSupports[:level+1]
        elif level > len(allRoots):
            logger.warning('Gauss data only available up to level %s. Resetting maximal level per dimension accordingly.', len(allRoots))

        collPt = {}
        basisSupports = {}
        for dim in range(self.d):
            collPt[dim] = []
            basisSupports[dim] = []

        for rootsPerLevel in allRoots:
            for dim, domainSupport in self.domain.iteritems():
                transformed_roots =  map(partial( \
                                         transform_from_to, self.defaultDomain, domainSupport),\
                                         rootsPerLevel)
                collPt[dim].append(transformed_roots)

        for supportsPerLevel in allSupports:
            for dim, domainSupport in self.domain.iteritems():
                perLevel = []
                for support in supportsPerLevel:
                    transformed_support = map(partial( \
                                          transform_from_to, self.defaultDomain, domainSupport),\
                                          support)
                    perLevel.append(transformed_support)
                basisSupports[dim].append(perLevel)

        return collPt, basisSupports 

    def _loadData_fromHD(self):
        path = os.path.dirname(__file__)
        dataFile = os.path.join(path, 'GaussData.json')
        with open(dataFile) as f:
            GaussData = json.load(f)
        coord1D = GaussData['coord1D']
        supports1D = GaussData['supports1D']
        assert len(coord1D) == len(supports1D)
        return coord1D, supports1D
