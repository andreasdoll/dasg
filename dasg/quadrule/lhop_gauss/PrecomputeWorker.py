from __future__ import division, absolute_import
import gc
from joblib import Parallel, delayed

from dasg.quadrule.lhop_gauss.utils import find_lower_roots, compute_point

class PrecomputeWorker(object):
    """ Worker for precomputation of gauss 
        collocation points and supports.
    """
    def __init__(self, nCPU):
        self.nCPU = nCPU
        self.all_roots = None
        self.all_boundaries = None

    def work(self, all_roots, all_supports, supports_onLevel, l):
        points = Parallel(self.nCPU)(delayed(\
                          _computePoint)(all_roots, all_supports, support, l) \
                          for support in supports_onLevel)
        return sorted(points) 

def _computePoint(all_roots, all_supports, support, l):
    roots = find_lower_roots(all_roots, all_supports, support)
    assert len(roots) == l
    point = compute_point(roots, support)
    assert point > support[0] and point < support[1]
    gc.collect()
    return point
