from __future__ import division, absolute_import
import logging

logger = logging.getLogger(__name__)

class LocalHighOrderPolynomial(object):
    """ Local high order polynomials as in [Dir00] """
    def __init__(self, d, collPt, supports):
        self._d = d
        self._collPt = collPt
        self._supports = supports

    @property
    def d(self):
        return self._d

    @property
    def coord1D(self):
        return self._coord1D

    @property
    def resizeFactor(self):
        return self._resizeFactor

    @property
    def supports1D(self):
        return self._supports1D

    @property
    def domain(self):
        return self._domain

    def evaluate(self, l, i, x):
        result = 1
        for j in range(self.d):
            result *= self.evaluate1D(l[j], i[j], x[j])
        return result

    def volume(self, l, i):
        result = 1
        for j in range(self.d):
            result *= self._volume1D(l[j], i[j])
        return result


