from __future__ import division, absolute_import
import logging
import os
import json

from dasg.quadrule.lhop_gauss.PrecomputeWorker import PrecomputeWorker
from dasg.quadrule.lhop_gauss.utils import create_new_supports

logger = logging.getLogger(__name__)

imported = {}
try:
    import sympy
    imported['sympy'] = True
except ImportError:
    imported['sympy'] = False


def precompute_data(level, nCPU):
    if imported['sympy'] == False:
        logger.error('Cannot precompute Gauss data, sympy is not available')
        raise ImportError('Cannot precompute Gauss data, sympy is not available')

    all_roots =      [[], [0]      ]
    all_boundaries = [[], [-1, 1]  ]
    all_supports =   [[], [[-1, 1]]]

    worker = PrecomputeWorker(nCPU)

    for l in range(1, level):
        supports_onLevel = create_new_supports(all_roots[l], all_boundaries[l])
        roots_onLevel = worker.work(all_roots, all_supports, supports_onLevel, l)

        all_roots.append(sorted(roots_onLevel))
        all_boundaries.append(sorted(all_boundaries[l]+all_roots[l]))
        all_supports.append(supports_onLevel)
    
        dump_roots = [map(lambda r: float(r), rpl) for rpl in all_roots]
        dump_supports = [[map(lambda b: float(b), bndry) for bndry in spl] for spl in all_supports] 

        _writeData_toHD(dump_roots, dump_supports)
        logger.info('Data for level %s precomputed', l)

def _writeData_toHD(roots, supports):
    path = os.path.dirname(__file__)
    dataFile = os.path.join(path, 'GaussData.json')
    with open(dataFile, 'w') as f:
        json.dump({'coord1D': roots, \
                   'supports1D': supports},
                  f)
