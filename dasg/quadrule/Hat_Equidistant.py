from __future__ import division, absolute_import
import numpy as np
import logging

from dasg.abc.QuadratureRule import QuadratureRule
from dasg.domain import transform_from_to
from dasg.quadrule.utils import resizeFactors
#from dasg.quadrule.equidistant_hat.ModifiedHat import ModifiedHat

logger = logging.getLogger(__name__)

class Hat_Equidistant(QuadratureRule):
    """ Implements hat basis functions as in [Pfl10]
        with equidistant collocation points
    """ 

    def __init__(self, function, maxLevel=11):
        self._defaultDomain = [0, 1]     # never modify
        #self._d = function.d
        self._domain = function.domain
        self._collPt = self._compute_collPt(maxLevel, function.domain)
        self._resizeFactors = resizeFactors(self._defaultDomain, function.domain, function.d) 
    
    @property
    def defaultDomain(self):
        return self._defaultDomain

    #@property
    #def d(self):
    #    return self._d

    @property
    def domain(self):
        return self._domain

    @property
    def collPt(self):
        return self._collPt

    @property
    def resizeFactors(self):
        return self._resizeFactors

    def volume(self, l, i):
        res = 1
        for j in range(self.d):
            res *= _volume_on_defaultDomain(l[j], i[j]) * self.resizeFactors[j]
        return res

    def _compute_collPt(self, maxLevel, supports):
        defaultDomain = self.defaultDomain
        collPt = {}
        for j, domain in self.domain.iteritems():
            pts_alloc = []
            for l in range(maxLevel+1):
                defaultCoords = _defaultCoord_upToLevel(l)
                transformedCoords = map(lambda x: transform_from_to(defaultDomain, domain, x), defaultCoords)
                pts_alloc.append(transformedCoords)
    
            coord1D_perDimension = []
            for l in range(maxLevel+1):
                coord1D_perDimension.append(_differenceGrids(l, pts_alloc))
            collPt[j] = coord1D_perDimension
        return collPt

    def evaluate(self, l, i, coord):
        x = self._to_defaultDomain(coord) 
        result = 1
        for j in range(self.d):
            result *= _evaluate_on_defaultDomain(l[j], i[j], x[j])
        return result

    def _to_defaultDomain(self, x):
        onDefault = np.zeros(self.d)
        for j, support in self.domain.iteritems():
            onDefault[j] = transform_from_to(support, self.defaultDomain, x[j])
        return onDefault

def _evaluate_on_defaultDomain(l, i, x):
    """ 1D evaluation at a point on the default domain"""
    if l == 1 and i == 1:
        return 1
    elif l > 1 and i == 1:
        if 0 <= x <= 1/(np.power(2, l-1)):
            return (2 - np.power(2, l)*x)
        else:
            return 0
    elif l > 1 and i == (np.power(2, l) - 1):
        if (1 - 1/(np.power(2, l-1))) <= x <= 1:
            return (np.power(2, l)*x + 1 - i)
        else:
            return 0
    else:
        return _hat_on_defaultDomain(l, i, x) 
  
def _volume_on_defaultDomain(l, i):
    """ 1D volume on default domain"""
    if l == 1 and i == 1:
        return 1
    elif l > 1 and (i == 1 or i == np.power(2, l) - 1):
        return 1/np.power(2, l-1)
    else:
        return 1/np.power(2, l) 

def _defaultCoord_upToLevel(l):
    """ Generates 1D coordinates of gridpoints """
    h = np.power(2, -float(l))
    pts = [j*h for j in range(1, np.power(2, l))]
    return pts

def _differenceGrids(l, all_coords):
    """ Generates difference grids for given levels.
        That is: newly added 1D-coordinates for given level 
    """
    if l == 0:
        Omega = set(all_coords[l])
    else:
        Omega = set(all_coords[l]) - set(all_coords[l-1])
    return sorted(list(Omega))

def _hat_on_defaultDomain(l, i, x):
    """ 1D hat basis on default domain """
    def stdHat(x):
        return max(1-np.abs(x), 0)
    return stdHat(np.power(2, l)*x - i)

