from __future__ import division
import numpy as np
from multiprocessing import cpu_count
import unittest
import random
from customlogger import setup_logger_at

from dasg import Grid
from dasg.quadrule import Equidistant_Hat, Gauss_LHOP
from dasg.levelworker import LevelWorker
from dasg.mcworker import FEM as MCWorker 
from dasg.tests.utils import data_decreases, qr_performance, approx_quality
from examples.Poisson.Poisson import Poisson


imported = {}
try:
    from alea.application.egsz.sample_domains import SampleDomain
    from alea.math_utils.multiindex import Multiindex
    from alea.math_utils.multiindex_set import MultiindexSet
    from alea.application.egsz.sample_problems2 import SampleProblem
    imported['alea'] = True
except ImportError: 
    imported['alea'] = False


class Test_FEM(unittest.TestCase):
    def setUp(self):
        criterion = {'method': 'maxIter',
                     'value': 500,
                    }
    
        self.QuadSettings = {'d': random.randint(4, 5),
                             'level': 15,
                             'maxLevel_perDimension': 11,
                             'adaptiveWeight': 1,
                             'convergence': criterion,
                             'verbose': True
                           }
    
        self.MCSettings = {'nMC': 500,
                           'nCPU': cpu_count()
                          }

        boundary_nr = 2
        domain_type = "square"
        problem_type = 0
        coeff_field = SampleProblem.setupCF("EF-square-cos",
                                            decayexp=2, 
                                            gamma=0.9,
                                            freqscale=1, 
                                            freqskip=0, 
                                            rvtype="uniform", 
                                            scale=1, 
                                            secondparam=None)

        # Solver
        dof = 30 
        degree_FEM = 2
        mesh0, boundaries, _ = SampleDomain.setupDomain(domain_type, initial_mesh_N=dof)
        mesh = SampleProblem.setupMesh(mesh0, num_refine=0)
    
        mis = [Multiindex(mis) for mis in MultiindexSet.createCompleteOrderSet(self.QuadSettings['d'], 0)]
    

        pde, bndry_Dirichlet, uD, bndry_Neumann, g, f = SampleProblem.setupPDE(boundary_nr,
                                                                               domain_type,
                                                                               problem_type,
                                                                               boundaries,
                                                                               coeff_field)
    
        w = SampleProblem.setupMultiVector(mis, pde, mesh, degree_FEM)
        self.solver = Poisson(self.QuadSettings['d'], mesh, coeff_field, pde, w)    


        # Reference solver
        refDim = 50
        dof = 40 
        degree_FEM = 3
        mesh0, boundaries, _ = SampleDomain.setupDomain(domain_type, initial_mesh_N=dof)
        mesh = SampleProblem.setupMesh(mesh0, num_refine=0)
    
        mis = [Multiindex(mis) for mis in MultiindexSet.createCompleteOrderSet(refDim, 0)]
    

        pde, bndry_Dirichlet, uD, bndry_Neumann, g, f = SampleProblem.setupPDE(boundary_nr,
                                                                               domain_type,
                                                                               problem_type,
                                                                               boundaries,
                                                                               coeff_field)
    
        w = SampleProblem.setupMultiVector(mis, pde, mesh, degree_FEM)
        self.referenceSolver = Poisson(refDim, mesh, coeff_field, pde, w)    


    @unittest.skipIf(not imported['alea'], "ALEA not available")
    def test_FEM(self):
        Hat = Equidistant_Hat(self.QuadSettings['d'], \
                              self.QuadSettings['maxLevel_perDimension'], \
                              self.solver.domain)
        LHOP = Gauss_LHOP(self.QuadSettings['d'], \
                          self.QuadSettings['maxLevel_perDimension'], \
                          self.solver.domain)
        QuadRules = [Hat, LHOP]
        levelWorker = LevelWorker(self.MCSettings['nCPU'])
        mcWorker = MCWorker(self.MCSettings, self.referenceSolver)

        tolerances = [1E-2, 1E-3, 1E-4]
        rmse = np.zeros((len(tolerances), 2))
    
        for r, QR in enumerate(QuadRules):
            for t, tol in enumerate(tolerances):
                self.QuadSettings['tol'] = tol
                grid = Grid(self.QuadSettings, QR, self.solver, levelWorker)
                grid.approximate()

                mcWorker.prepare(grid)
                coll_error = mcWorker.work()
                print coll_error
                rmse[t, r] = coll_error 

        print rmse
        self.assertTrue(data_decreases(rmse, 'Hat'))
        self.assertTrue(data_decreases(rmse, 'LHOP'))
        self.assertTrue(qr_performance(rmse))
        self.assertTrue(approx_quality(rmse, tolerances, 'Hat'))
        self.assertTrue(approx_quality(rmse, tolerances, 'LHOP'))


if __name__ == '__main__':
    logger = setup_logger_at('.')
    unittest.main()
