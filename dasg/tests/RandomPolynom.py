from __future__ import division, absolute_import
#import numpy as np

from dasg.abc import Solver as AbstractSolver

class RandomPolynom(AbstractSolver):
    def __init__(self, polynom, domain):
        self._polyDict = polynom.as_dict()
        self._d = len(self._polyDict.keys()[0])
        self._dof = 1
        self._domain = domain

    @property
    def d(self):
        return self._d

    @property
    def dof(self):
        return self._dof

    @property
    def domain(self):
        return self._domain

    def evaluate(self, x, supplementaryObjects=None):
        pDict = self._polyDict
        result = 0
        for exponents, coeff in pDict.iteritems():
            term = 1
            for c, e in enumerate(exponents):
                term *= x[c]**e
            term *= int(coeff)
            result += term

        return [result]

