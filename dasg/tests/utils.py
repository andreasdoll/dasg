from __future__ import division, absolute_import
import random

def data_decreases(data, basis):
    nTol = data.shape[0]
    if basis == 'Hat':
        c = 0
    elif basis == 'LHOP':
        c = 1

    for j in range(1, nTol):
        if data[j-1, c] < data[j, c]:
            return False

    return True

def approx_quality(data, tols, basis):
    if basis == 'Hat':
        c = 0
    elif basis == 'LHOP':
        c = 1
    for d, t in zip(data, tols):
        if d[c] > t:
            return False
    return True

def qr_performance(data):
    print data
    print data.shape[0]
    for j in range(data.shape[0]):
        hat = data[j][0]
        lhop = data[j][1]
        if hat < lhop:
            return False
    return True

def create_randomSupport(d):
    domain = {}
    for j in range(d):
        a = random.uniform(-2, 2)
        b = random.uniform(-2, 2)
        support = sorted([a,b])
        domain[j] = support
    return domain
