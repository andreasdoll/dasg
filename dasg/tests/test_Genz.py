from __future__ import division, absolute_import
import numpy as np
import unittest
from multiprocessing import cpu_count
import random
from customlogger import setup_logger_at

from dasg import Grid
from dasg.quadrule import Equidistant_Hat, Gauss_LHOP
from dasg.levelworker import LevelWorker
from dasg.mcworker import Simple as MCWorker
from dasg.tests.utils import data_decreases, qr_performance, approx_quality
from examples.Genz.Genz import Genz

class Test_Genz(unittest.TestCase):
    def setUp(self):
        criterion = {'method': 'maxIter',
                     'value': 500,
                    }
    
        self.QuadSettings = {'level': 15,
                             'maxLevel_perDimension': 11,
                             'adaptiveWeight': 1,
                             'convergence': criterion,
                             'verbose': True
                           }
    
        self.MCSettings = {'nMC': 500,
                           'nCPU': cpu_count()
                          }

    def test_Gaussian(self):
        d = random.randint(1, 10)
        self.QuadSettings['d'] = d

        solver = Genz(self.QuadSettings['d'], 'gaussian')
        Hat = Equidistant_Hat(self.QuadSettings['d'], \
                              self.QuadSettings['maxLevel_perDimension'], \
                              solver.domain)
        LHOP = Gauss_LHOP(self.QuadSettings['d'], \
                          self.QuadSettings['maxLevel_perDimension'], \
                          solver.domain)
        QuadRules = [Hat, LHOP]
        levelWorker = LevelWorker(self.MCSettings['nCPU'])
        mcWorker = MCWorker(self.MCSettings, solver, solver.domain)

        tolerances = [1E-2, 1E-3, 1E-4]
        rmse = np.zeros((len(tolerances), 2))
        vole = np.zeros((len(tolerances), 2))
    
        for r, QR in enumerate(QuadRules):
            for t, tol in enumerate(tolerances):
                self.QuadSettings['tol'] = tol
                grid = Grid(self.QuadSettings, QR, solver, levelWorker)
                grid.approximate()
                volume = grid.integrate()[0]
                symbolic_volume = solver.symbolic()

                vol_error = np.abs(volume - symbolic_volume)
                vole[t, r] = vol_error
                self.assertLess(vol_error, tol)

                mcWorker.prepare(grid)
                coll_error = mcWorker.work()
                
                rmse[t, r] = coll_error 

        print vole
        print rmse
        self.assertTrue(data_decreases(rmse, 'Hat'))
        self.assertTrue(data_decreases(rmse, 'LHOP'))
        self.assertTrue(data_decreases(vole, 'Hat'))
        self.assertTrue(data_decreases(vole, 'LHOP'))
        self.assertTrue(qr_performance(vole))
        self.assertTrue(qr_performance(rmse))
        self.assertTrue(approx_quality(vole, tolerances, 'Hat'))
        self.assertTrue(approx_quality(vole, tolerances, 'LHOP'))
        self.assertTrue(approx_quality(rmse, tolerances, 'Hat'))
        self.assertTrue(approx_quality(rmse, tolerances, 'LHOP'))

    def test_ProductPeak(self):
        d = random.randint(1, 10)
        self.QuadSettings['d'] = d

        solver = Genz(self.QuadSettings['d'], 'productPeak')
        Hat = Equidistant_Hat(self.QuadSettings['d'], \
                              self.QuadSettings['maxLevel_perDimension'], \
                              solver.domain)
        LHOP = Gauss_LHOP(self.QuadSettings['d'], \
                          self.QuadSettings['maxLevel_perDimension'], \
                          solver.domain)
        QuadRules = [Hat, LHOP]
        levelWorker = LevelWorker(self.MCSettings['nCPU'])
        mcWorker = MCWorker(self.MCSettings, solver, solver.domain)

        tolerances = [1E-2, 1E-3, 1E-4]
        rmse = np.zeros((len(tolerances), 2))
        vole = np.zeros((len(tolerances), 2))
    
        for r, QR in enumerate(QuadRules):
            for t, tol in enumerate(tolerances):
                self.QuadSettings['tol'] = tol
                grid = Grid(self.QuadSettings, QR, solver, levelWorker)
                grid.approximate()
                volume = grid.integrate()[0]
                symbolic_volume = solver.symbolic()

                vol_error = np.abs(volume - symbolic_volume)
                vole[t, r] = vol_error

                mcWorker.prepare(grid)
                coll_error = mcWorker.work()
                
                rmse[t, r] = coll_error 

        print vole
        print rmse
        self.assertTrue(data_decreases(rmse, 'Hat'))
        self.assertTrue(data_decreases(rmse, 'LHOP'))
        self.assertTrue(data_decreases(vole, 'Hat'))
        self.assertTrue(data_decreases(vole, 'LHOP'))
        self.assertTrue(qr_performance(vole))
        self.assertTrue(qr_performance(rmse))
        self.assertTrue(approx_quality(vole, tolerances, 'Hat'))
        self.assertTrue(approx_quality(vole, tolerances, 'LHOP'))
        self.assertTrue(approx_quality(rmse, tolerances, 'Hat'))
        self.assertTrue(approx_quality(rmse, tolerances, 'LHOP'))


if __name__ == '__main__':
    logger = setup_logger_at('.')
    unittest.main()
