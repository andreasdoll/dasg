from __future__ import division, absolute_import
import numpy as np
import unittest
from multiprocessing import cpu_count
import random
from customlogger import setup_logger_at

from dasg import Grid
from dasg.quadrule import Equidistant_Hat, Gauss_LHOP
from dasg.levelworker import LevelWorker
from dasg.mcworker import Simple as MCWorker
#from dasg.domain import hyper
from dasg.tests.utils import create_randomSupport, data_decreases, qr_performance, approx_quality
from examples.Polynom.Polynom import Polynom
from RandomPolynom import RandomPolynom

imported = {}
try:
    from sympy import Poly, S, integrate
    from sympy.abc import a,b,c,d,e,f,g,h,i,j
    imported['sympy'] = True
except ImportError():
    imported['sympy'] = False


class Test_Polynom(unittest.TestCase):
    def setUp(self):
        criterion = {'method': 'maxIter',
                     'value': 500,
                    }
    
        self.QuadSettings = {'level': 15,
                             'maxLevel_perDimension': 11,
                             'adaptiveWeight': 1,
                             'convergence': criterion,
                             'verbose': True
                           }
    
        self.MCSettings = {'nMC': 500,
                           'nCPU': cpu_count()
                          }

    def test_1DPolynom(self):
        self.QuadSettings['d'] = 1
        domain = create_randomSupport(self.QuadSettings['d'])

        solver = Polynom(domain) 
        Hat = Equidistant_Hat(self.QuadSettings['d'], \
                              self.QuadSettings['maxLevel_perDimension'], \
                              domain)
        LHOP = Gauss_LHOP(self.QuadSettings['d'], \
                          self.QuadSettings['maxLevel_perDimension'], \
                          domain)
        QuadRules = [Hat, LHOP]
        levelWorker = LevelWorker(self.MCSettings['nCPU'])
        mcWorker = MCWorker(self.MCSettings, solver, domain)

        tolerances = [1E-2, 1E-3, 1E-4]
        rmse = np.zeros((len(tolerances), 2))
        vole = np.zeros((len(tolerances), 2))
    
        for r, QR in enumerate(QuadRules):
            for t, tol in enumerate(tolerances):
                self.QuadSettings['tol'] = tol
                grid = Grid(self.QuadSettings, QR, solver, levelWorker)
                grid.approximate()
                volume = grid.integrate()[0]
                symbolic_volume = solver.symbolic()
                print volume, symbolic_volume

                vol_error = np.abs(volume - symbolic_volume)
                vole[t, r] = vol_error
                self.assertLess(vol_error, tol)

                mcWorker.prepare(grid)
                coll_error = mcWorker.work()
                
                rmse[t, r] = coll_error 
        
        print vole
        print rmse
        self.assertTrue(data_decreases(rmse, 'Hat'))
        self.assertTrue(data_decreases(rmse, 'LHOP'))
        self.assertTrue(data_decreases(vole, 'Hat'))
        self.assertTrue(data_decreases(vole, 'LHOP'))
        self.assertTrue(qr_performance(vole))
        self.assertTrue(qr_performance(rmse))
        self.assertTrue(approx_quality(vole, tolerances, 'Hat'))
        self.assertTrue(approx_quality(vole, tolerances, 'LHOP'))
        self.assertTrue(approx_quality(rmse, tolerances, 'Hat'))
        self.assertTrue(approx_quality(rmse, tolerances, 'LHOP'))

    @unittest.skipIf(not imported['sympy'], "sympy is not available")
    def test_randomPolynom(self):
        dim = random.randint(2, 8)
        self.QuadSettings['d'] = dim
        domain = create_randomSupport(dim)
        variables = [a,b,c,d,e,f,g,h,i,j][:dim]
        nTerms = random.randint(4, 12)
        support = []
        for var in range(dim):
            supp = domain[var]
            support.append((variables[var], supp[0], supp[1]))

        pCoeff = {}
        for n in range(nTerms):
            expo = np.repeat(0, dim)
            expo = tuple(map(lambda x: random.randint(0, 3), expo))
            pCoeff[expo] = S(random.randint(-10, 10))

        polynom = Poly(pCoeff, variables)
        solver = RandomPolynom(polynom, support)
        for v in range(dim):
            var = variables[v]
            polynom = integrate(polynom.as_expr(), (var, domain[v][0], domain[v][1]))

        Hat = Equidistant_Hat(self.QuadSettings['d'], \
                              self.QuadSettings['maxLevel_perDimension'], \
                              domain)
        LHOP = Gauss_LHOP(self.QuadSettings['d'], \
                          self.QuadSettings['maxLevel_perDimension'], \
                          domain)
        QuadRules = [Hat, LHOP]
        levelWorker = LevelWorker(self.MCSettings['nCPU'])
        mcWorker = MCWorker(self.MCSettings, solver, domain)

        tolerances = [1E-1, 1E-2, 1E-3]
        rmse = np.zeros((len(tolerances), 2))
        vole = np.zeros((len(tolerances), 2))
    
        for r, QR in enumerate(QuadRules):
            for t, tol in enumerate(tolerances):
                self.QuadSettings['tol'] = tol
                grid = Grid(self.QuadSettings, QR, solver, levelWorker)
                grid.approximate()
                volume = grid.integrate()[0]

                vol_diff = np.abs(volume - polynom)/100
                vole[t, r] = vol_diff

                mcWorker.prepare(grid)
                coll_error = mcWorker.work()
                
                rmse[t, r] = coll_error 

        print vole
        print rmse
        self.assertTrue(data_decreases(rmse, 'Hat'))
        self.assertTrue(data_decreases(rmse, 'LHOP'))
        self.assertTrue(data_decreases(vole, 'Hat'))
        self.assertTrue(data_decreases(vole, 'LHOP'))
        self.assertTrue(qr_performance(vole))
        self.assertTrue(qr_performance(rmse))
        self.assertTrue(approx_quality(vole, tolerances, 'Hat'))
        self.assertTrue(approx_quality(vole, tolerances, 'LHOP'))
        self.assertTrue(approx_quality(rmse, tolerances, 'Hat'))
        self.assertTrue(approx_quality(rmse, tolerances, 'LHOP'))

    logger = setup_logger_at('.')
    unittest.main()
