from setuptools import setup, find_packages

setup(
    name='dasg',
    description='Dimension adaptive sparse grids for numerical integration and interpolation',
    long_description='This package is intended for numerical integration and interpolation of functions mapping from compact and possibly high-dimensional domains onto arbitrary finite-dimensional spaces. It implements dimension-adaptive sparse Smolyak grids with variable quadrature rules.',
    version='0.7',
    author='Andreas Doll',
    author_email='fuseki@posteo.de',
    url='http://bitbucket.org/andreasdoll/dasg',
    classifiers=[
        'Development Status :: 4 - Beta',
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Mathematics",
        ],
    keywords='sparse grid, Smolyak, dimension adaptive, integration, quadrature, interpolation, collocation',
    license='MIT',
    install_requires=[
        'setuptools',
        'numpy'
        ],
    packages=find_packages(),
    )
